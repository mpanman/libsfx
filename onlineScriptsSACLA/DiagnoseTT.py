import os,re,sys,optparse,glob,datetime,collections
import os.path as osp
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

import subprocess as sbp

if sys.version_info.major==3:
    TTmodFn = osp.abspath('./TTclasses')
else:
    TTmodFn = osp.abspath('./')

if not TTmodFn in sys.path:
    sys.path.insert(0, TTmodFn)

import TTclasses as tc

def loadTT(opts, run):
    print("TT for run {0}".format(run))
    pthObj = tc.PathTT(opts.iniFile)
    pthObj.loadZero()
    
    TT = tc.RunTT(pthObj)
    TT.loadTT(run)
    TT.loadMopo()
    TT.includeMissingTags()
    TT.getCalibrationFromFile()
    TT.timeFromCalibration()

    TT.plotDiagnostics(plot_untagged=opts.miss, kind=str(opts.sty))

    return TT

def main():

    def get_comma_separated_args(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))

    parser = optparse.OptionParser()

    parser.add_option("-i", "--inifile", action="store", type="string",
                      metavar='INI', dest='iniFile', default='ini_0.ini',
                    help="Path of the ini file to be loaded.")

    parser.add_option("-s", "--runStart", action="store", type='int',
                      metavar='RUNSTART', dest='rs', default=0,
                    help="First run in range.")

    parser.add_option("-f", "--runEnd", action="store", type='int',
                      metavar='RUNEND', dest='re', default=0,
                    help="Last run in range.")

    parser.add_option("-m", "--missing", action="store_true", 
                      metavar='MISSING', dest='miss', default=False,
                    help="If True, plots the untagged shots too at pixel position 0.")

    parser.add_option("-d", "--style", action="store", type='string',
                      metavar='STYLE', dest='sty', default='average',
                    help="Histograms to plot the peak position. Possible options: average, derivative, fit, all")



    opts,args = parser.parse_args()

    if not opts.iniFile:
        raise IOError("-i or --inifile option required!")

    Run = {}
    #poo
    for run in np.arange(opts.rs, opts.re+1,1):
        Run[run] = loadTT(opts, run)
        
    plt.show(block=False)


    
    return Run

if __name__ == "__main__":
    #pthObj,metaObj = init_sys()
    Run = main()
    sys.exit(0)
