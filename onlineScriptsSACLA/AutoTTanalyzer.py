import os,re,sys,optparse,glob,time,datetime,collections, socket
import os.path as osp
import numpy as np
#import matplotlib as mpl
#import matplotlib.pyplot as plt

import subprocess as sbp

if sys.version_info.major==3:
    TTmodFn = osp.abspath('/work/sebastian/scriptsTT/TTclasses')
else:
    TTmodFn = osp.abspath('./')

if not TTmodFn in sys.path:
    sys.path.insert(0, TTmodFn)

import TTclasses as tc

#pathObj = tc.PathTT('./ini_0.ini')
#meta    = tc.MetaTT(pathObj)


def init_sys(iniFile):
    print("\n### INITIALIZING ###")
    print("Host: {0}".format(socket.gethostname()))
    if 'xhpcfep01'!=socket.gethostname() and 'e840'!=socket.gethostname():
        print("Not on a normal node!!!")
        print("### FAILED INITIALIZATION ###")
        sys.exit(1)

    else:
        pthObj  = tc.PathTT(iniFn=iniFile)
        #pthObj.loadZero()

        metaObj = tc.MetaTT(pthObj)
        #print("test 1:{0}".format(getattr(metaObj, 'currentRun')))
        if getattr(metaObj, 'currentRun')==0:
            metaObj.requestCurrentRun()
            metaObj.writeCurrentRun()
        else:
            metaObj.readCurrentRun()
        
        #try:
            #metaObj.readLastProcTagMopo()
        #except OSError:
    

        metaObj.getLastMopoRun()
        metaObj.getLastProcRun()
        metaObj.getLastTagsRun()
        metaObj.getLastTTRun()
        metaObj.writeLastProcTagMopo()
            
        metaObj.getAvailableProcRuns()
        #metaObj.getEventList()
        #metaObj.makeEventList(recreate=True)
        
        #print("test1: {0}".format(metaObj.availableRunsProc))
        #print("test2: {0}".format(metaObj.availableRunsEvents))
        #sys.exit(0)
        print("### INITIALIZED ###\n")

    return pthObj,metaObj

def sleepRun(opts):
    sys.stdout.write("Up to date... Sleeping for:")
    for i in range(int(opts.sleep),0,-1):
        sys.stdout.write(str(i)+' ')
        sys.stdout.flush()
        time.sleep(1)
    sys.stdout.write("\n")
    return

def getMetaRun(opts):

    pthObj  = tc.PathTT(iniFn=opts.iniFile)
    metaObj = tc.MetaTT(pthObj)

    # Get current run
    metaObj.requestCurrentRun()

    # Write current run
    #metaObj.readCurrentRun()
    metaObj.readLastProcTagMopo()
    metaObj.getAvailableProcRuns()
    #metaObj.getEventList()
    #metaObj.getLastEvent()
    metaObj.getLastTTRun()
    metaObj.getLastProcRun()
    #print("test3: {0}".format(metaObj.availableRunsEvents))
    
    return metaObj

def TTanalyze(opts):
    
    metaObj = getMetaRun(opts)
    print("testTT1: {0}".format(metaObj.availableRunsProc))
    metaObj.getLastTTRun()
    #print("testTT2: {0}, {1}".format(metaObj.availableRunsTT, metaObj.lastRunProc))
    #sys.exit(0)
    metaObj.requestTMAoffline(reanalyze=opts.ra)

    return

def genEventList(opts):

    metaObj = getMetaRun(opts)
    metaObj.getAvailableProcRuns()
    metaObj.getEventList()
    metaObj.makeEventList(recreate=opts.ra)

    return

def sentinel(opts):

    print("### TT ANALYSIS STARTING... ###\n")
    metaObjL = getMetaRun(opts)
    print("### TT ANALYSIS ENTERING CYCLES ###\n")
    c = 1

    while True:
        print("### TT TAG MOPO MONITOR {0} ###\n".format(c))
        metaObj = getMetaRun(opts)
        #metaObj.readCurrentRun()
        #bork
        #print("test4: {0}".format(metaObj.availableRunsEvents))
        #if opts.ra:
            ##metaObj.writeTagsMopoPerRun(overwrite=True)
            
            #print("Finished reanalyzing.")
            #sys.exit(0)
        #print("Proc: {0} ; Event: {1}".format(metaObj.lastRunProc,metaObj.lastRunEvent))
        print("test5: {0}".format(metaObj.availableRunsProc))
        #if (metaObj.lastRunProc > metaObj.lastRunEvent) or opts.ra:
            #print("### EVENT GENERATION START ###\n")
            ##bork
            #genEventList(opts)
            #print("### EVENT GENERATION DONE ###\n")

        #print("noTT?")
        print("test6: {0} {1}".format(metaObjL.lastRunProc, metaObj.lastRunTT))
        
        if (metaObjL.lastRunProc > metaObj.lastRunTT) or opts.ra:
            if not opts.noTT:
                print("### TT ANALYSIS START ###\n")
                TTanalyze(opts)
                print("### TT ANALYSIS DONE ###\n")
        else:
            if not opts.ra:
                sleepRun(opts)
            else:
                print("### FINISHED REANALYZING ###")
                return



        print("### END OF CYCLE {0} ###\n".format(c))
        metaObjL = metaObj
        c+=1
    return


def main():

    def get_comma_separated_args(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))

    parser = optparse.OptionParser()

    parser.add_option("-i", "--inifile", action="store", type="string",
                      metavar='INI', dest='iniFile', default=None,
                    help="Path of the ini file to be loaded.")

    parser.add_option("-s", "--sleep", action="store", type='int',
                      metavar='SLEEP', dest='sleep', default=20,
                    help="Length of time in seconds to wait for when the script is caught up.Length of time in seconds to wait for when the script is caught up.")
    
    parser.add_option("-r", "--reanalyze", action="store_true",
                      metavar='REANALYZE', dest='ra', default=False,
                    help="Resubmits all the TTjobs and list generation for each run starting from the first run.")
    
    parser.add_option("-n", "--noTT", action="store_true",
                      metavar='NOTT', dest='noTT', default=False,
                    help="Skips submitting the TT jobs if the reananlyze flag is active.")
    
    parser.add_option("-w", "--writeini", action="store", type='str',
                      metavar='WRITEINI', dest='wi', default=False,
                    help="Path of the ini file to be written. If the flag is active, the script will only write the inifile and exit.")

    
    opts,args = parser.parse_args()

    if not opts.iniFile:
        raise IOError("-i or --inifile option required!")

    pthObjIni,metaObjIni = init_sys(opts.iniFile)
    
    if opts.wi:
        pthObjIni.writeIniFile(altIni=opts.wi)
        print("Written ini file: {0}".format(opts.wi))
        sys.exit(0)
    if opts.ra:
        print("### REANALYZING ###")
    sentinel(opts)
    
    return 

if __name__ == "__main__":
    #pthObj,metaObj = init_sys()
    main()
    sys.exit(0)
