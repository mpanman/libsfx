import os,re,sys,argparse,glob,datetime,collections
import os.path as osp
import numpy as np
#import matplotlib as mpl
#import matplotlib.pyplot as plt

import subprocess as sbp

if sys.version_info.major==3:
    TTmodFn = osp.abspath('./TTclasses')
else:
    TTmodFn = osp.abspath('./')

if not TTmodFn in sys.path:
    sys.path.insert(0, TTmodFn)

import TTclasses as tc

pathObj = tc.PathTT('./ini_0.ini')
meta    = tc.MetaTT(pathObj)

meta.readCurrentRun()
meta.getLastProcRun()
meta.getLastTTRun()
meta.getLastTagsRun()
meta.getLastMopoRun()

print("Proc: {0} TT: {1} Tags: {2} Mopo: {3}".format(meta.lastRunProc, meta.lastRunTT, meta.lastRunTags, meta.lastRunMopo))

force = np.arange(647708, 647712+1, 1)

#force = np.array([646213]) 
#meta.readLastProcTagMopo()
#meta.writeLastProcTagMopo()

meta.requestMopoFromRun(force)
meta.writeTagsMopoPerRun()

#meta.requestTMAoffline()
