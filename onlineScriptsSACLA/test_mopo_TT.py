import os,re,sys,argparse,glob,datetime,collections
import os.path as osp
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

import subprocess as sbp

if sys.version_info.major==3:
    TTmodFn = osp.abspath('./TTclasses')
else:
    TTmodFn = osp.abspath('./')

if not TTmodFn in sys.path:
    sys.path.insert(0, TTmodFn)

import TTclasses as tc

pathObj = tc.PathTT('./ini_0.ini')

par = tc.ParTT(pathObj)
par.loadSampleList()

runs = list(range(718043, 718055,1))

par.pairRunEventTT(plot=False, force=runs)

bins = (np.arange(1.8, 2.5, 0.1)*1000).astype(int)
bins = np.asarray([10000.])
Sample = par.binRunEventTT(bins = None)



par.writePairedRunEventTT(postfix='fs')
