import os
import subprocess as sp
import glob
import sys
import MySQLdb
import time 
import commands

''' Hopefully more robust autosubmitter that does not submit gibberish 
    scan numbers and upset the HPC crew. Gomenasai!'''

# Parameters to set:
h5path = '/work/asod/hdf5'
#sensible_range = range(644838, 670000)
sensible_range = range(647021, 670000)  # changed since we need to delete old junk hdf5 due to quota 

# Function to get latest finished scan
# The MySQLdb can sometimes (often) not be reached, then just wait a bit. 
def get_latest_run():
    try:
        #connect to MySQL server
        connector=MySQLdb.connect(host="xqmydb01",db="XEXP000",user="daqadmin",passwd="mysql")
        cursor=connector.cursor()
        cursor.execute("select run_number from RunInfo order by run_number desc limit 1;")
        result=cursor.fetchall()
        
        #close connection
        cursor.close()
        connector.close()
        
        a=result[0]
        b=a[0]
    except:
        b = -1
    
    return int(b)

# Function to check if scan is in queue:
# This can be refined to not break e.g. if the PBS $JOBID and scan number are the same..
def check_in_queue(scan):
    cmd = sp.Popen('qstat -a | grep '+str(scan), 
                   shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    out, err = cmd.communicate()
    return len(out) > 0, out, err  # if out is not empty, scan should be in queue


# Continuously run
stuff_is_not_updated = True
while True:
    if stuff_is_not_updated:
        # get last created hdf5 file:
        print 'Getting H5 list...'
        sorted_h5s = sorted(glob.glob(h5path+'/*.h5'), key=os.path.getctime)
        latest_h5 = sorted_h5s[-1]

    try:
        latest_h5_number = int(latest_h5.split('/')[-1].split('.h5')[0])
    except:
        print 'Something whent wrong when scanning h5 dir, some weird stuff in there?'
        latest_h5_number = -1
    
    # get last scan
    last_scan = get_latest_run()
    print 'Last Scan: '+str(last_scan)
    
    if last_scan not in sensible_range:
        print 'MySQL DB down... waiting a bit ...'
        time.sleep(3)
        continue
    
    if latest_h5_number not in sensible_range:
        print 'Latest HDF5 file: {0:d} weird! Check hdf5 dir'.format(latest_h5_number)
        print 'I think this is the last file created: {0:s}.format'(sorted_h5s[-1])
        continue
    
    stuff_is_not_updated = False
    # If everything is sensible, make range of scans that do not have hdf5 files:
    non_transled_scans = range(latest_h5_number, last_scan+1) # Python indexing ahem
    print 'Scans to check: '+str(non_transled_scans)
    
    # Loop over them, check if they are queue, if not, submit:
    for this_scan in non_transled_scans:
   	# Check if the hdf5 file already exists and skip if it does:
	if h5path+'/'+str(this_scan)+'.h5' in sorted_h5s:
            print 'HDF5 file already exists for {0:d}, skipping ...'.format(this_scan)
            non_transled_scans.remove(this_scan)
	    continue
 
        # Check if in queue
        print 'Checking if in queue'
        in_queue, qstat_output, __ = check_in_queue(this_scan)
        if not in_queue:
            print 'Submitting: '+str(this_scan)+'...'
            output = commands.getoutput('./SACLA_Process.sh '+str(this_scan))
            # Print whats going on to screen:
            print output
        else:  # It was already found in queue:
            print '{0:d} scan already found in queue:'.format(this_scan)
            print qstat_output
            non_transled_scans.remove(this_scan)

    time.sleep(0.5)
    stuff_is_not_updated = True
