import os,re,sys,optparse,glob,time,datetime,collections, socket
import os.path as osp
import numpy as np
#import matplotlib as mpl
#import matplotlib.pyplot as plt

import subprocess as sbp

if sys.version_info.major==3:
    TTmodFn = osp.abspath('./TTclasses')
else:
    TTmodFn = osp.abspath('./')

if not TTmodFn in sys.path:
    sys.path.insert(0, TTmodFn)

import TTclasses as tc

def write(opts,data):
    print("Writing tags and motor positions: {0}".format(opts.outFile))
    hdr = " tags "+" ".join(opts.motor)
    np.savetxt(opts.outFile, data, fmt='%.0f')
    return

def loadMotorNames(opts):
    pathObj = tc.PathTT(opts.iniFile)
    fn = osp.abspath(osp.join(pathObj.pathMeta, pathObj.mopoNameFn))
    mopoNames = []
    with open(fn, 'r') as fp:
        for line in fp:
            mopoNames.append(line)
    mopoNames=np.asarray(mopoNames, dtype=str)        
    return mopoNames

def request(opts):
    import dbpy
    motorNames = opts.motor
    bl         = opts.bl
    run        = opts.run
    motor = []
    
    mopoNames=loadMotorNames(opts)
    
    if dbpy.read_runstatus(bl, run)==0:
        
        print("\nRequesting tags and mopo\'s for run: {0}".format(run))
        trigger_hi = int(dbpy.read_hightagnumber(bl, run))
        tags     = dbpy.read_taglist_byrun(bl, run)
        motor.append(tags)
        for mot in opts.motor:
            #if mot in mopoNames:
            motor.append(np.asarray(dbpy.read_syncdatalist_float(mot, trigger_hi, tags), dtype=int))
            
            #else:
                #print("Motor name {0} not in equipment list!".format(mot))
    else:
        print("Run {0} not finished!".format(run))

    data = np.asarray(motor).T
    #print(data.shape)

    return data

def main():
    def get_comma_separated_args(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))

    parser = optparse.OptionParser()

    parser.add_option("-i", "--inifile", action="store", type="string",
                      metavar='INI', dest='iniFile', default='ini_0.ini',
                    help="Path of the ini file to be loaded.")

    parser.add_option("-o", "--outfile", action="store", type="string",
                      metavar='OUT', dest='outFile', default='mopos.dat',
                    help="File name of the mopos of the specified run. Default: mopos.dat")

    parser.add_option("-m", "--motor", action="callback", callback=get_comma_separated_args,
                      metavar='MOTOR', dest='motor', default='', type="string",
                    help="Comma separated, no spaces, list of motor names. e.g. xfel_bl_3_st_2_motor_1,xfel_bl_3_st_1_motor_73/position,xfel_bl_3_lh1_shutter_1_open_valid/status,xfel_bl_3_st_2_motor_2/position .")

    parser.add_option("-r", "--run", action="store", type='int',
                      metavar='RUN', dest='run', default=0,
                    help="Run number.")

    parser.add_option("-b", "--beamline", action="store", type='int',
                      metavar='BL', dest='bl', default=3,
                    help="Beamline. Default: 3")
    

    opts,args = parser.parse_args()
    
    if not opts.iniFile:
        raise IOError("-i or --inifile option required!")

    data = request(opts)

    write(opts,data)
    
    return data

if __name__ == "__main__":
    main()
    sys.exit(0)
