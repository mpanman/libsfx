import os,re,sys,argparse,glob,datetime,collections
import os.path as osp
import numpy as np
#import matplotlib as mpl
#import matplotlib.pyplot as plt

import subprocess as sbp

if sys.version_info.major==3:
    TTmodFn = osp.abspath('./TTclasses')
else:
    TTmodFn = osp.abspath('./')

if not TTmodFn in sys.path:
    sys.path.insert(0, TTmodFn)

import TTclasses as tc


pathObj = tc.PathTT('./ini_1.ini')
meta    = tc.MetaTT(pathObj)

run = 717414


meta.readCurrentRun()
meta.getLastProcRun()
meta.getLastTTRun()
meta.getLastTagsRun()
meta.getLastMopoRun()
meta.getLastTagsRunCheetah()


pathObj_0 = tc.PathTT('./ini_1.ini')
metaObj_0 = tc.MetaTT(pathObj)
metaObj_0.readCurrentRun()
metaObj_0.getLastProcRun()
metaObj_0.getLastTTRun()
metaObj_0.getLastTagsRun()
metaObj_0.getLastMopoRun()