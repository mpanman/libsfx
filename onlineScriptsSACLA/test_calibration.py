import os,re,sys,glob,datetime,collections
import os.path as osp
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

import subprocess as sbp

if sys.version_info.major==3:
    TTmodFn = osp.abspath('./TTclasses')
else:
    TTmodFn = osp.abspath('./')

if not TTmodFn in sys.path:
    sys.path.insert(0, TTmodFn)

import TTclasses as tc

pathObj = tc.PathTT('./ini_0.ini')
pathObj.loadZero()

runs = list(range(717944,717961,1))
#+list(range(717964,717967))+list(range(717970,717974))

cal = tc.CalTT(pathObj)
cal.loadTT(runs, absolute_mopo=False)
cal.calibrate(pixFitLim=[600, 1250.], plot=True)
cal.writeCalib(fromRun=pathObj.firstRun, overWrite=True)
plt.show(block=False)