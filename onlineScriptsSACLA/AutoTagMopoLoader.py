import os,re,sys,optparse,glob,time,datetime,collections, socket
import os.path as osp
import numpy as np
#import matplotlib as mpl
#import matplotlib.pyplot as plt

import subprocess as sbp

if sys.version_info.major==3:
    TTmodFn = osp.abspath('./TTclasses')
else:
    TTmodFn = osp.abspath('./')

if not TTmodFn in sys.path:
    sys.path.insert(0, TTmodFn)

import TTclasses as tc

def init_sys(iniFile):
    print("\n### INITIALIZING ###")
    if not 'hpc01-clst' in socket.gethostname():
        print("Not on an interactive qsub node!!!")
        print("### FAILED INITIALIZATION ###")
        sys.exit(1)

    else:
        pthObj  = tc.PathTT(iniFn=iniFile)
        #pthObj.loadZero()

        metaObj = tc.MetaTT(pthObj)
        metaObj.requestCurrentRun()
        metaObj.writeCurrentRun()
        
        metaObj.getLastProcRun()
        metaObj.getLastTTRun()
        metaObj.getLastTagsRun()
        metaObj.getLastMopoRun()

        metaObj.writeLastProcTagMopo()
        print("### INITIALIZED ###\n")

    return pthObj,metaObj

def sleepRun(opts):
    sys.stdout.write("Up to date... Sleeping for:")
    for i in range(int(opts.sleep),0,-1):
        sys.stdout.write(str(i)+' ')
        sys.stdout.flush()
        time.sleep(1)
    sys.stdout.write("\n")
    return

def getMetaRun(opts):

    pthObj  = tc.PathTT(iniFn=opts.iniFile)
    metaObj = tc.MetaTT(pthObj)

    # Get current run
    metaObj.requestCurrentRun()
    # Write current run
    #read last Proc TT Tags Mopo file
    metaObj.getLastProcRun()
    metaObj.getLastTTRun()
    metaObj.getLastTagsRun()
    metaObj.getLastMopoRun()

    return metaObj

def sentinel(opts):
    
    print("### TT TAG MOPO MONITOR STARTING... ###")
    metaObjL = getMetaRun(opts)
    print("### TT TAG MOPO MONITOR ENTERING CYCLES ###")
    c = 1

    while True:
        print("### TT TAG MOPO MONITOR {0} ###".format(c))
        metaObj = getMetaRun(opts)
        metaObj.requestCurrentRun()

        if opts.ra:
            force = np.arange(metaObj.firstRun, metaObj.currentRun, 1)
            for run in force:
                _sleep = metaObj.requestMopoFromRun(force=run, overwrite=True)
                if _sleep:
                    sleepRun(opts)                
                    continue
                else:
                    metaObj.writeTagsMopoPerRun(overwrite=True)
            print("Finished reanalyzing.")
            sys.exit(0)
        elif c==1:
            #metaObj = getMetaRun(opts)

            _sleep = metaObj.requestMopoFromRun(from_first=True)
            if _sleep:
                sleepRun(opts)                
                continue
            else:
                metaObj.writeTagsMopoPerRun()

        elif metaObj.currentRun > metaObjL.lastRunMopo:
            print(metaObj.currentRun, metaObjL.lastRunMopo)
            #metaObj = getMetaRun(opts)

            _sleep = metaObj.requestMopoFromRun()
            if _sleep:
                sleepRun(opts)                
                continue
            else:
                metaObj.writeTagsMopoPerRun()
            sleepRun(opts)
            
        else:
            sleepRun(opts)

        metaObj.writeLastProcTagMopo()
        metaObj.writeCurrentRun()


        print("### END OF CYCLE {0} ###\n".format(c))
        metaObjL = metaObj
        c+=1

    return

def main():

    def get_comma_separated_args(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))

    parser = optparse.OptionParser()

    parser.add_option("-i", "--inifile", action="store", type="string",
                      metavar='INI', dest='iniFile', default=None,
                    help="Path of the ini file to be loaded.")

    parser.add_option("-s", "--sleep", action="store", type='int',
                      metavar='SLEEP', dest='sleep', default=20,
                    help="Length of time in seconds to wait for when the script is caught up.")
    
    parser.add_option("-r", "--reanalyze", action="store_true",
                      metavar='REANALYZE', dest='ra', default=False,
                    help="Redo the tag and mopo request etc. for each run starting at teh first run. WARNING will take a long time at the end of the beam time.")

    parser.add_option("-w", "--writeini", action="store", type='str',
                      metavar='WRITEINI', dest='wi', default=False,
                    help="Path of the ini file to be written. If the flag is active, the script will only write the inifile and exit.")

    
    opts,args = parser.parse_args()

    if not opts.iniFile:
        raise IOError("-i or --inifile option required!")

    pthObjIni,metaObjIni = init_sys(opts.iniFile)
    
    if opts.wi:
        pthObjIni.writeIniFile(altIni=opts.wi)
        print("Written ini file: {0}".format(opts.wi))
        sys.exit(0)

    sentinel(opts)
    
    return 

if __name__ == "__main__":
    #pthObj,metaObj = init_sys()
    main()
    sys.exit(0)
