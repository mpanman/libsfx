import os,re,sys,argparse,glob,datetime,collections
import os.path as osp
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt

import subprocess as sbp

if sys.version_info.major==3:
    TTmodFn = osp.abspath('./TTclasses')
else:
    TTmodFn = osp.abspath('./')

if not TTmodFn in sys.path:
    sys.path.insert(0, TTmodFn)

import TTclasses as tc


def init_sys():
    pthObj  = tc.PathTT(iniFn='./iniTT.ini')
    pthObj.loadZero()

    metaObj = tc.MetaTT(pthObj)
    metaObj.requestCurrentRun()
    
    metaObj.getLastMopoRun()
    metaObj.getLastTagsRunCheetah()

    metaObj.writeCurrentRun()
    metaObj.writeLastProcTagMopo()

    return pathObj,metaObj

def run_meta():

    pthObj  = tc.PathTT(iniFn='./iniTT.ini')
    pthObj.loadZero()

    metaObjr = tc.MetaTT(pthObj)
    # read meta files
    metaObjr.requestCurrentRun()
    metaObjr.readLastProcTagMopo()

    metaObjr.getMissingTagsCheetah()

    p = metaObjr.sendRequestMopo(dummy=False)
    metaObjr.readRequestedMopo()

    metaObjr.writeTagsMopoRobust(overWrite=True)
    
    return metaObjr

def check_in_queue(run):
    cmd = sbp.run(['qstat','-a', '|', 'grep'+str(run)], 
                   shell=True, stdout=sp.PIPE, stderr=sp.PIPE)
    #out, err = cmd.communicate()
    return len(cmd.stdout) > 0, cmd.stdout, cmd.stderr  # if out is not empty, scan should be in queue


def sentinel():
    while True:
        
        run_meta()

        time.sleep(0.5)

def main():
    
    return
    
if __name__ == "__main__":
    #pthObj,metaObj = init_sys()
    main()
    sys.exit(0)
