import os,re,sys,argparse,glob,datetime,collections
import os.path as osp
import numpy as np
#import matplotlib as mpl
#import matplotlib.pyplot as plt

import subprocess as sbp

if sys.version_info.major==3:
    TTmodFn = osp.abspath('./TTclasses')
else:
    TTmodFn = osp.abspath('./')

if not TTmodFn in sys.path:
    sys.path.insert(0, TTmodFn)

import TTclasses as tc

