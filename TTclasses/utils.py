import os,re,sys,argparse,glob,datetime,collections
import os.path as osp
import numpy as np
#import matplotlib as mpl
#import matplotlib.pyplot as plt

def getIdx():
    return

def readTagFile(pathFn):
    return np.genfromtxt(pathFn, delimiter='', skip_header=6, dtype=int)

def readMopoFile(pathFn):
    return np.genfromtxt(pathFn, delimiter='', skip_header=0, dtype=float)

def setNan2Val(data, mask, val):
    dat = data.copy()
    dat[mask] = val
    return dat

def readCheetahCSV(fn):
    """
    FILENAME,NPEAK,NCRYST,EVENT,NREFL,RESO,A,B,C,ALPHA,BETA,GAMMA
    """
    return np.genfromtxt(fn, delimiter=',', skip_header=1, dtype=str)

def extractCheetahCSV(path, run):

    base,fn = osp.split(path)

    rawCht  = np.atleast_2d(readCheetahCSV(path))

    if rawCht.size==0:
        return None

    run     = rawCht[:,0]
    hit     = rawCht[:,2].astype(bool)
    tags    = np.asarray([tag.lstrip('tag-').rstrip('//') for tag in rawCht[:,3]], dtype=int)

    return (run, hit, tags)

def genDummyTTcsv(run, tagL, path):
    #import scipy as sp

    fn = osp.join(path)

    tagL = np.asarray(tagL)

    numShot = tagL.size

    dist_f     = np.random.normal(loc=800, scale=250, size=numShot).astype(np.float)
    dist_d     = np.random.normal(loc=800, scale=250, size=numShot).astype(np.float)
    #numNanf    = np.random.randint(0.,dist_f.size)
    #numNand    = np.random.randint(0.,dist_d.size)
    dist_d_nan = np.random.normal(loc=800, scale=250, size=dist_d.size).astype(np.int)
    dist_f_nan = np.random.normal(loc=800, scale=250, size=dist_f.size).astype(np.int)

    dist_d[dist_d_nan>800./np.random.randint(1.,2.)] = np.nan
    dist_f[dist_f_nan>800./np.random.randint(1.,2.)] = np.nan

    dummy = np.hstack((tagL,dist_d,dist_f), dtyle=object)
    np.savetxt(fn, dummy)

    return

def read_bad(fnDir):
    rawBad = []
    with open(fnDir, 'r') as fp:
        for line in fp:
            lin = line.split()
            if lin[0]=='#':
                continue
            else:
                rawBad.append(lin[0])

    return np.asarray(rawBad, dtype=int).tolist()

def gen_lst_tmp(fn, processed_files_list):

    if not isinstance(processed_files_list, collections.Iterable) or type(processed_files_list)==str:
        pfL = [processed_files_list]

    with open(fn, 'w') as fp:
        for pf in pfL:
            if osp.isfile(pf):
                fp.write('{0}\n'.format(pf))

    return

def write_TMAoffline(fn, out):
    with open(fn, 'w') as fp:
        fp.write(out)
    print("Written TMAoffline output in: {0}".format(fn))
    
def readEventsList(fn, run, test):

    rexRun = re.compile(r'{0}'.format(test))
    tag = []

    with open(fn, 'r') as fp:
        for line in fp:
            lin = line.split()
            _fn = osp.split(lin[0])[-1]
            #_fn = lin[0]
            if not rexRun.match(_fn):
                print("Fail 1")
                return
            if int(rexRun.match(_fn).groups()[0])!=int(run):
                print("Fail 2")
                return
            elif int(rexRun.match(_fn).groups()[0])==int(run):
                rn  = run
                tag.append(lin[1].lstrip('tag-').rstrip('//'))
            else:
                raise StandardError
    return np.asarray(tag, dtype=int)
