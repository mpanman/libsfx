import os,re,sys,optparse
import os.path as osp
import numpy as np


def main():

    def get_comma_separated_args(option, opt, value, parser):
        setattr(parser.values, option.dest, value.split(','))

    parser = optparse.OptionParser()

    #parser.add_option("-t", "--tags", action="callback", type="string", dest='tags',
                    #metavar='TAGS', default=None, callback=get_comma_separated_args,
                    #help="Tag list. Format should be \'<tag0>,<tag1>,...,<tagn>\'. Use single quotes, and no spaces!")

    parser.add_option("-t", "--tags", action="store", type="string", dest='tags',
                    metavar='TAGS', default=None, 
                    help="File containing tags.")

    parser.add_option("-m", "--motors", action="callback", type="string", dest='motors',
                      metavar='MOTORS', default=None, callback=get_comma_separated_args,
                      help=". E.g. \'xfel_bl_3_st_2_motor_1/position,xfel_bl_3_st_1_motor_73/position\' ")

    parser.add_option("-s", "--hi", action="store", type="int", dest='hi', default=None,
                      metavar='HI', help="Trigger_hi value.")


    parser.add_option("-o", "--outfile", action="store", type="string",
                      metavar='OUT', dest='outfile', default=None,
                    help="")

    opts,args = parser.parse_args()

    try:
        import dbpy
        setattr(opts, 'dummy', False)
    except ImportError:
        setattr(opts, 'dummy', True)
        print("Dummy mode.")

    return (opts,args)


def readTags(opts,args):
    tags = np.loadtxt(opts.tags, dtype=int)
    return tags


def dbRequest(opts, args):

    trigger_hi = opts.hi
    tags       = np.genfromtxt(opts.tags, dtype=int)
    motors     = opts.motors
    mopo       = []
    for mot in motors:
        if not opts.dummy:
            mopo.append(dbpy.read_syncdatalist_float(mot, trigger_hi, tags))
        else:
            mopo.append(np.zeros_like(tags))
        
    mopo = np.asarray(mopo, dtype=int)
    #print("Successfully received motor positions for {0} tags from:\n{1}".format(tags.size, '\n'.join(motors)))
    return (tags,mopo)

def writeMopo(opts, args, tags, mopo):
    hdr  = '# {0} {1}\n'.format('tags', ' '.join(opts.motors))
    mopo = mopo.astype(str).reshape(-1,len(opts.motors))

    #wrt = 'w' if not osp.isfile(opts.outfile) else 'a'

    with open(opts.outfile, 'w') as fp:
        fp.write(hdr)
        for i,tag in enumerate(tags):
            fp.write('{0} {1}\n'.format(tag, ' '.join(mopo[i])))
    #print("Written")

    return

if __name__ == "__main__":

    (opts, args) = main()
    tags         = readTags(opts,args)
    tags,mopo    = dbRequest(opts, args)
    writeMopo(opts, args, tags, mopo)
    sys.exit(0)


