import os,re,sys,glob,datetime,collections
import os.path as osp
import numpy as np
import subprocess as sbp
import socket

if 'e840'==socket.gethostname():
    import matplotlib as mpl
    mpl.use('tkAgg')
    import matplotlib.pyplot as plt
else:
    import matplotlib as mpl
    mpl.use('Agg')
    import matplotlib.pyplot as plt

from utils import *

# bl3_st2_pm001 : nominal time motor (Mopo 1, DTU) xfel_bl_3_st_2_motor_1/position (quick database query)
# bl3_st1_pm073 : TT laser w.r.t. Xray motor (Mopo 73, DTU) xfel_bl_3_st_1_motor_73/position (quick database query)

class PathTT:
    def __init__(self, iniFn='./iniTT.ini'):
        self._attr_names     = ['paths','files','file_patterns','dir_patterns',
                                'experiment_info','programs','motors']

        self.paths           = ['pathTag','pathTT','pathProc','pathMeta','pathPart','pathScripts',
                                'pathMopo', 'pathConf', 'pathTTjobs']
        self.files           = ['calTTfn','partLstfn','lastMopoTagfn','currentRunfn','mopoNameFn',
                                'dbMopofn','sampleListfn', 'tagReqfn','zeroMopofn', 'confTTfn','badRunfn', 'geomFn']
        self.file_patterns   = ['fnCsvPattern', 'fnTagPattern','fnMopPattern',
                                'fnStrpattern', 'fnRnIpattern', 'drStrpattern',
                                'fnTMApattern','fnLEpattern','fnTTMPattern',
                                'fnPartpattern']
        self.dir_patterns    = ['dirDescriptors']
        self.experiment_info = ['beamline', 'firstRun', 'mopo2fs','trigger_hi',
                                'currentRun']
        self.programs        = ['dbRequestScript','ttMonAnalyzer','qsubSubmit','pyVer',
                                'listEvents']
        self.motors          = ['laserON','laserOFF','nomTmotor','TTmotor', 'NDmotor']

        self._attr         = self.paths + self.files + self.programs + self.file_patterns + self.dir_patterns + self.experiment_info + self.motors

        self.iniFn = osp.abspath(iniFn)

        self.currentRun = 0
        self.loadPathFromFile()
        return

    def loadPathManually(self, **kwargs):
        self._unused = {}
        for el in kwargs.keys():
            if el in self._attr:
                setattr(self, el, kwargs[el])
            else:
                self._unused.update({el:kwargs[el]})

        print('These kwargs were not used! {0}'.format(', '.join(self._unused.keys()))) if len(self._unused.keys())!=0 else None

        self.checkPaths()

        return

    def loadPathFromFile(self, altIni=None):

        if type(altIni)!=type(None):
            self.iniFn = osp.abspath(altIni)

        self._unused = {}
        with open(self.iniFn, 'r') as fp:
            for line in fp:
                _line = line.split()
                if len(_line)==2:
                    if _line[0] in self._attr:
                        setattr(self, _line[0], _line[1])
                    else:
                        self._unused.update({_line[0]:_line[1]})


        print('These kwargs were not used! {0}'.format(', '.join(self._unused.keys()))) if len(self._unused.keys())!=0 else None

        for elem in self.dir_patterns:
            setattr(self, elem, getattr(self, elem).split(','))

        self.checkPaths()

        return

    def writeIniFile(self, altIni=None):

        if type(altIni)!=type(None):
            self.iniFn = osp.abspath(altIni)

        with open(self.iniFn, 'w') as fp:
            for field in self._attr_names:
                fp.write('[{0}]\n'.format(field))
                for elem in getattr(self, field):
                    fp.write('{0:15} {1}\n'.format(elem, ','.join(getattr(self, elem)) if elem in self.dir_patterns else getattr(self, elem)))

        print('Successfully Written ini file: {0}'.format(self.iniFn))

        return

    def checkPaths(self):
        for pth in self.paths:
            if not osp.isdir(getattr(self,pth)):
                raise OSError("{0} path does not exist: {1}".format(pth, getattr(self,pth)))
            else:
                setattr(self,pth,osp.abspath(getattr(self,pth)))

        for mot in self.motors:
            setattr(self, mot, str(getattr(self,mot)))

        if type(self.beamline)!=int:
            self.beamline = int(self.beamline)

        if type(self.firstRun)!=int:
            self.firstRun = int(self.firstRun)

        if type(self.trigger_hi)!=int:
            self.trigger_hi= int(self.trigger_hi)

        if type(self.currentRun)!=int:
            self.currentRun= int(self.currentRun)

        #if type(self.nomTmotor)!=str:
            #self.nomTmotor = str(self.nomTmotor)

        #if type(self.TTmotor)!=str:
            #self.TTmotor = str(self.TTmotor)

        if type(self.mopo2fs)!=float:
            self.mopo2fs = float(self.mopo2fs)

        return

    def loadZero(self):
        fn = osp.join(self.pathMeta, self.zeroMopofn)
        self.zeroMopo = np.atleast_2d(np.genfromtxt(fn))
        return

    ### Metadata handling ###


class RunTT:
    """
    """
    def __init__(self, pthObj):
        """
        """
        #bork
        if not isinstance(pthObj, PathTT):
            raise TypeError("")
        else:
            self.path = pthObj

        if not np.isclose(self.path.mopo2fs, 6.666666666667, rtol=1e-3):
            raise Warning("kwarg mopo2fs motor position to femtosecond conversion is off from 6.666666666667.")

        return


    def loadTT(self, runNum):
        self.runNum = int(runNum)
        self._detZeroMopo()

        fn = osp.join(self.path.pathTT,self.path.fnCsvPattern%self.runNum)

        print("Loading TT data for run {0}: {1}".format(runNum, fn))

        if not osp.isfile(fn):
            raise OSError("TT csv file not found: {0}".format(fn))

        rawData  = np.genfromtxt(fn, skip_header=2, delimiter=',')
        self.tags     = rawData[:,0].astype(int)
        self.Ntags    = self.tags.size
        self.fltpos_d = rawData[:,1]
        self.fltpos_f = rawData[:,2]
        #self.fltpos_a = np.nanmean(rawData[:,1:], axis=1)
        self._isTT_d  = ~np.isnan(self.fltpos_d)
        self._isTT_f  = ~np.isnan(self.fltpos_f)
        self.fltpos_a = np.ones_like(self.fltpos_d)*np.nan
        self._isTT_a  = (self._isTT_f*self._isTT_d)
        self.fltpos_a = np.nanmean(np.vstack((self.fltpos_f,
                                              self.fltpos_d)).T, axis=1)
        #self._isTT_a = ~np.isnan(self.fltpos_a)

        self.isTT    = self._isTT_a
        #self._isTT_a = self._isTT_d | self._isTT_f


        self._mean_fltpos_d = np.nanmean(self.fltpos_d)
        self._mean_fltpos_f = np.nanmean(self.fltpos_f)
        self._mean_fltpos_a = np.nanmean(self.fltpos_a)
        print("Successfully loaded TT data for run {0}".format(runNum))

        return

    def _detZeroMopo(self):

        mask = self.path.zeroMopo[:,0] <= self.runNum
        self.t0_m = self.path.zeroMopo[mask][-1,1]
        self.TTt0_m = self.path.zeroMopo[mask][-1,2]

        return

    def loadMopo(self):
        """
        Loads motor positions from mopo tags file.
        Requires _detZeroMopo() method to have been run or manualMopo() method.
        """

        fn      = osp.join(self.path.pathMopo, self.path.fnMopPattern%self.runNum)
        rawMopo = np.loadtxt(fn, dtype=int)


        # Not all tags are recorded in the TT csv file!!! Dafuq!?
        #if not np.all(self.tags == rawMopo[:,0]):
            #raise ValueError("")
        self._mopoTags       = rawMopo[:,0]
        self._idxMopoTags = self._mopoTags.searchsorted(self.tags)
        
        try:
            self._mopoXtra = rawMopo[:,3:]
        except:
            self._mopoXtra = None

        #self.tNom_m = np.unique(rawMopo[self._idxMopoTags,1]).mean()
        #self.TT_m = np.unique(rawMopo[self._idxMopoTags,2]).mean()

        self.tNom_m = rawMopo[self._idxMopoTags,1]
        
        #self.TT_m = rawMopo[self._idxMopoTags,2]
        self.TT_m = rawMopo[self._idxMopoTags,2] - self.TTt0_m

        #self.isLas  = np.unique(rawMopo[self._idxMopoTags,3]).mean()

        self._tNom_c_m  = self.tNom_m - self.t0_m
        self._tNom_c_fs = self._tNom_c_m * self.path.mopo2fs

        return

    def includeMissingTags(self):
        """
        """
        full_tNom_m    = np.ones_like(self._mopoTags)*np.nan
        full_TT_m    = np.ones_like(self._mopoTags)*np.nan
        full_tNom_c_fs = np.ones_like(self._mopoTags)*np.nan
        
        full_fltpos_d = np.ones_like(self._mopoTags)*np.nan
        full_fltpos_f = np.ones_like(self._mopoTags)*np.nan
        full_fltpos_a = np.ones_like(self._mopoTags)*np.nan

        full_tNom_m[self._idxMopoTags]    = self.tNom_m
        full_TT_m[self._idxMopoTags]    = self.TT_m
        full_tNom_c_fs[self._idxMopoTags] = self._tNom_c_fs

        full_fltpos_d[self._idxMopoTags] = self.fltpos_d
        full_fltpos_f[self._idxMopoTags] = self.fltpos_f
        full_fltpos_a[self._idxMopoTags] = self.fltpos_a


        self.tags     = self._mopoTags
        self.Ntags    = self.tags.size
        
        self.tNom_m     = full_tNom_m
        self.TT_m     = full_TT_m
        self._tNom_c_fs = full_tNom_c_fs
        
        self.fltpos_d = full_fltpos_d
        self.fltpos_f = full_fltpos_f
        self.fltpos_a = full_fltpos_a

        self._isTT_d = ~np.isnan(self.fltpos_d)
        self._isTT_f = ~np.isnan(self.fltpos_f)
        self._isTT_a = ~np.isnan(self.fltpos_a)
        self.isTT    = self._isTT_a

        return

    def manualMopo(self, verbose=False,
                 NominalTimeMotorPosition = None, ZeroTimeMotorPosition = None,
                 TTMotorPosition = None):

        if not isinstance(NominalTimeMotorPosition, float) and not isinstance(NominalTimeMotorPosition, int):
            raise TypeError("NominalTimeMotorPosition kwarg needs to be an int or a float!")
        else:
            self.tNom_m = np.ones_like(self.tags)*int(NominalTimeMotorPosition)

        if not isinstance(ZeroTimeMotorPosition, float) and not isinstance(ZeroTimeMotorPosition, int):
            raise TypeError("ZeroTimeMotorPosition kwarg needs to be an int or a float!")
        else:
            self.t0_m = np.ones_like(self.tags)*int(ZeroTimeMotorPosition)

        if not isinstance(TTMotorPosition, float) and not isinstance(TTMotorPosition, int):
            raise TypeError("TTMotorPosition kwarg needs to be an int or a float!")
        else:
            self.TT_m = np.ones_like(self.tags)*int(TTMotorPosition)

        if (type(offset)!=type(None) and type(slope)==type(None)) or (type(slope)!=type(None) and type(offset)==type(None)):
            raise TypeError("Either both offset and slope kwargs are Nonetypes, or both are floats.")

        return

    def getCalibrationFromFile(self):

        fn = osp.join(self.path.pathMeta, self.path.calTTfn)
        rawCal = np.atleast_2d(np.genfromtxt(fn))
        mask = rawCal[:,0] <= self.runNum

        self.cal_slope, self.cal_offset = rawCal[mask][-1,1],rawCal[mask][-1,2]

        return

    def manualCalibration(self, offset=None, slope=None):

        if not isinstance(offset, float) and not isinstance(offset, int):
            #raise TypeError("Both offset and slope kwargs need to be floats or Nonetypes.")
            print("offset needs to be set in order to calculate the time.") if verbose else None
        else:
            self.cal_offset = offset

        if not isinstance(slope, float) and not isinstance(slope, int):
            #raise TypeError("Both offset and slope kwargs need to be floats or Nonetypes.")
            print("slope needs to be set in order to calculate the time.")# if verbose else None
        else:
            self.cal_slope  = slope

        return

    def timeFromCalibration(self):
        """
        kwargs:
            kind (str) : 
                'gaussian' : 
                'mean'     : 
                'median'   : 
        """

        if not (hasattr(self, 'cal_offset') and hasattr(self, 'cal_slope')):
            raise AttributeError("offset and slope need to be defined!")
        else:
            o,s = self.cal_offset,self.cal_slope

        #self._fltpos_d_fs = (self.fltpos_d - self._mean_fltpos_d) * self.cal_slope
        #self._fltpos_f_fs = (self.fltpos_f - self._mean_fltpos_f) * self.cal_slope
        #self._fltpos_a_fs = (self.fltpos_a - self._mean_fltpos_a) * self.cal_slope

        self._fltpos_d_fs = (self.fltpos_d - self.TT_m) * self.cal_slope
        self._fltpos_f_fs = (self.fltpos_f - self.TT_m) * self.cal_slope
        self._fltpos_a_fs = (self.fltpos_a - self.TT_m) * self.cal_slope

        self.t_a = self._tNom_c_fs - self._fltpos_a_fs
        self.t_d = self._tNom_c_fs - self._fltpos_d_fs 
        self.t_f = self._tNom_c_fs - self._fltpos_f_fs 

        #self.t_a[~self._isTT_a] = 
        #bork

        return

    def plotDiagnostics(self, kind='average', plot_untagged=True, binN=50, **kwargs):
        """
        Plots a histogram of the individual shots as a function of pixel number (pix) and time if a calibration is defined.
        kwargs:
            plot_untagged (bool)  : If True, plot all shots not tagged by TT at 0 pixels.
            binN          (int)   : Number of bins, default is 50.
            kind          (str)   : can take the values:
                                        'derivative : Plots a histogram as a function of the pixels as from the derivative (fltpos_d).
                                        'fit'       : Plots a histogram as a function of the pixels as from the fit (fltpos_f).
                                        'average'   : Default. Plots a histogram as a function of the average of the derivative (fltpos_d) and fit (fltpos_f).
                                        'all'       : Plots a histogram as a function of all the above.
        """

        #def add_t():
            #return

        fig,ax = plt.subplots(figsize=(6.5,5.5))
        fig.suptitle('Run {0} tagged shots: {1}'.format(self.runNum, np.sum(self.isTT)))
        cycle = np.array([mpl.colors.to_rgba(col) for col in ('C0','C1','C2')])
        cycle[:,-1] = 0.5

        binN   = int(binN)
        figDat = {}
        data   = []
        D      = {}
        switch = 'raw' if plot_untagged else 'data'

        if kind=='fit' or kind=='all':

            D['fit'] = {'data': self.fltpos_f[self._isTT_f],
                        'raw' : setNan2Val(self.fltpos_f, ~self._isTT_f, 0.),
                        'untagged': np.sum(~self._isTT_f)}
            data.append(D['fit'][switch])

        if kind=='derivative' or kind=='all':
            D['derivative'] = {'data': self.fltpos_d[self._isTT_d],
                               'raw' : setNan2Val(self.fltpos_d, ~self._isTT_d, 0.),
                               'untagged': np.sum(~self._isTT_d)}
            data.append(D['derivative'][switch])

        if kind=='average' or kind=='all':
            D['average'] = {'data': self.fltpos_a[self._isTT_a],
                            'raw' : setNan2Val(self.fltpos_a, ~self._isTT_a, 0.),
                            'untagged': np.sum(~self._isTT_a)}
            data.append(D['average'][switch])
        #bork

        bins = np.histogram(np.hstack(data), bins=binN)[1] #get the bin edges
        
        for i,key in enumerate(D.keys()):
            figDat[key] = ax.hist(D[key][switch], bins=bins, lw=1, fc=cycle[i],
                                  color=cycle[i], ec=cycle[i], 
                                  label='{0} untagged: {1}'.format(key, D[key]['untagged']))

        ax.axvline(x=self._mean_fltpos_a, ls=':',c='black')
        ax.text(np.min(ax.get_xlim()),np.max(ax.get_ylim()),
                'TTmean: {0:.0f}, TnomOffset: {1:.0f} pulses, Tnom: {2:.0f} pulses, Tnom: {3:.2f} ps'.format(self._mean_fltpos_a, np.nanmean(self.t0_m), np.nanmean(self._tNom_c_m), (np.nanmean(self._tNom_c_fs)/1000.)), va='bottom',ha='left')

        ax.legend()
        ax.xaxis.set_major_locator(mpl.ticker.MaxNLocator(nbins=10,
                                                          #steps=[1,2,5,10],
                                                          ))
        if hasattr(self, 'cal_slope'):
            axMoPos = ax.get_xticks()
            axPs    = (np.nanmean(self._tNom_c_fs) - (axMoPos - np.nanmean(self.TT_m)) * self.cal_slope )/1000.

            ax.set_xticklabels([u'{0:.0f}\n{1:.2f}'.format(el,axPs[j]) for j,el in enumerate(axMoPos)])

            ax.set_xlabel('pix / ps')
        else:
            ax.set_xlabel('pix')

        #print("Debug TT dignose:")
        #print(np.nanmean(self._tNom_c_fs), self._mean_fltpos_a, self.cal_slope)
        #print("## Debug TT dignose ##")

        ax.set_ylabel('number of shots')

        self.diagnosticPlot       = fig
        self._diagnosticPlot_data = figDat

        return

class CalTT:
    """
    """
    def __init__(self, pthObj):
        """
        """
        #bork
        if not isinstance(pthObj, PathTT):
            raise TypeError("")
        else:
            self.path = pthObj

        if not np.isclose(self.path.mopo2fs, 6.666666666667, rtol=1e-3):
            raise Warning("kwarg mopo2fs motor position to femtosecond conversion is off from 6.666666666667.")

        return

    def loadTT(self, runs, absolute_mopo=False):

        calibrationRuns = np.sort(runs)
        
        if calibrationRuns.size==0:
            raise ValueError("runs must contain at least 2 elements.")

        cRL = np.arange(calibrationRuns.min(), calibrationRuns.max()+1, 1, dtype=int)

        self.calRun={}
        avL    = []
        mopoTTfs = []
        mopoTTm  = []

        for run in cRL:
            try:
                MyRun = RunTT(self.path)
                MyRun.loadTT(run)
                MyRun.loadMopo()
                avL.append(MyRun._mean_fltpos_a)
                c = 0 if absolute_mopo else MyRun.TTt0_m
                mopoTTfs.append(np.mean(MyRun.TT_m + c)*self.path.mopo2fs) # c is aded, because RunTT subtracts it
                mopoTTm.append(np.mean(MyRun.TT_m + c)) # c is aded, because RunTT
                self.calRun[run] = MyRun
                print("Added run {0} to calibration line.".format(run))
            except OSError:
                print("Run csv file for run {0} not found!".format(run))
        self.calLine_fs = np.vstack((avL,mopoTTfs))
        idxSort = np.argsort(self.calLine_fs[0,:])
        self.calLine_fs = self.calLine_fs[:,idxSort]

        self.calLine_m = np.vstack((avL,mopoTTm))
        idxSort = np.argsort(self.calLine_m[0,:])
        self.calLine_m = self.calLine_m[:,idxSort]

        return

    def calibrate(self, pixFitLim=[925., 1065.], mopo=True, plot=False):

        if type(pixFitLim)!=list and type(pixFitLim)!=np.array and type(pixFitLim)!=tuple:
            raise TypeError("pixFitLim needs to be a 2-element array-like!")
        else:
            self.pixFitLim = np.asarray(pixFitLim)
            
        if mopo:
            self.calLine = self.calLine_m
        else:
            self.calLine = self.calLine_fs
        self._mode_mopo = mopo

        self._fitMask = (self.calLine[0]<=np.max(pixFitLim))*(self.calLine[0]>=np.min(pixFitLim))

        self._outCal  = np.polyfit(self.calLine[0,self._fitMask], self.calLine[1,self._fitMask], 1)
        #bork
        if plot:
            self.plot()

        return

    def plot(self):

        fig,ax = plt.subplots()
        if self._mode_mopo:
            label = 'Fit:\nslope:{0} (pulse/pix)\nOffset:{1} (pulses)'.format(*self._outCal)
        else:
            label = 'Fit:\nslope:{0} (fs/pix)\nOffset:{1} (fs)'.format(*self._outCal)
        
        ax.plot(self.calLine[0], self.calLine[1], marker='o', ls='', color='C0')
        ax.plot(self.calLine[0][self._fitMask], np.polyval(self._outCal, self.calLine[0, self._fitMask]), ls='-', color='C3', label=label)
        ax.plot(self.calLine[0], np.polyval(self._outCal, self.calLine[0]), ls=':', color='C3', label='extrapolation')
        ax.set_ylabel('{0} ({1})'.format(*('mopo','pulses') if self._mode_mopo else ('time','fs')))
        ax.set_xlabel('detector pixel (pix)')
        runs = np.asarray(list(self.calRun.keys()), dtype=int)
        fig.suptitle('Runs: {0}-{1} calibration'.format(runs.min(),runs.max()))
        ax.legend()
        self.calPlot = fig

        return

    def writeCalib(self, fromRun=None, overWrite=False):

        if type(fromRun)!=type(None):
            self.fromRun = int(fromRun)
        elif hasattr(self, _latestRun):
            self.fromRun = self._latestRun
        else:
            self.fromRun = self._firstRun

        fn = osp.join(self.path.pathMeta, self.path.calTTfn)

        _hdr = '{0:12} {1:12} {2:12}\n'.format('# From run,', 'slope,', 'offset')
        line = '{0} {1} {2}\n'.format(self.fromRun, self._outCal[0], self._outCal[1])

        if not osp.isfile(fn) or overWrite:
            with open(fn, 'w') as fp:
                fp.write(_hdr)
                fp.write(line)
            print("Written calibration data to {0}".format(fn))
        else:
            with open(fn, 'a') as fp:
                fp.write(line)
            print("Appended calibration data to {0}".format(fn))
        return

class MetaTT:

    def __init__(self, pthObj):

        if not isinstance(pthObj, PathTT):
            raise TypeError("")
        else:
            self.path = pthObj

        self.currentRun = 0
        self.firstRun = self.path.firstRun
        #self.badRuns = []
        self.getBadRuns()
        return


    def getBadRuns(self):

        fnDir = osp.join(self.path.pathMeta, self.path.badRunfn)

        if not osp.isfile(fnDir):
            return
        else:
            rawBad = read_bad(fnDir)

        self.badRuns  = np.asarray(rawBad, dtype=int).tolist()

        return


    def recordBadRun(self, run, mssg):
        fnDir = osp.join(self.path.pathMeta, self.path.badRunfn)

        if not osp.isfile(fnDir):
            self.badRuns.append(run)
            with open(fnDir, 'w') as fp:
                fp.write("# run rejection\n")
                fp.write("{0:.0f} {1}\n".format(run, mssg))
            print("Created bad run file: {0}".format(fnDir))
        else:
            rawBad = read_bad(fnDir)
            #print(run)
            #print(rawBad)
            if not run in rawBad:
                self.badRuns.append(run)
                with open(fnDir, 'a') as fp:
                    fp.write("{0:.0f} {1}\n".format(run, mssg))
            else:
                #print("")
                pass


        return


    def requestCurrentRun(self):
        """
        Request Current Run from database with ShowNewestRunNumber
        """
        p = sbp.run(('ShowNewestRunNumber', '-b', str(self.path.beamline)),
                    stdin=sbp.PIPE, stdout=sbp.PIPE, stderr=sbp.PIPE, universal_newlines=True)
        #p = sbp.run(('ShowNewestRunNumber', '-b', '2'),  stdin=sbp.PIPE, stdout=sbp.PIPE, stderr=sbp.PIPE, universal_newlines=True)

        if p.returncode!=0:
            print("This call did not work: {0}".format(' '.join(p.args)))
            return

        self.currentRun = int(p.stdout)
        print("Retrieved current run: {0}".format(self.currentRun))

        return

    def readCurrentRun(self):
        """
        Read Current Run
        """

        fn = osp.join(self.path.pathMeta,self.path.currentRunfn)
        try:
            rawNum = np.loadtxt(fn, dtype=int)
        except OSError:
            print("Current run file does not exist! {0}".format(fn))
            return

        if not rawNum>=self.currentRun:
            raise ValueError("Current run {0} is less than the stored value {1}!".format(rawNum, self.currentRun))
        else:
            setattr(self, 'currentRun', rawNum)
            print("Read current run: {0}".format(self.currentRun))

        return

    def writeCurrentRun(self):

        fn = osp.join(self.path.pathMeta,self.path.currentRunfn)
        print("Saving current run file: {0} ...".format(fn))
        np.savetxt(fn, [self.currentRun], fmt='%.0f')

        return


    def getLastProcRun(self):
        """
        Get Last Processed Run from globbing the processed folder
        """
        fnDir = osp.join(self.path.pathProc, '*')
        drL  = glob.glob(fnDir)
        rexProc = re.compile(r'{0}'.format(self.path.drStrpattern%('(\d+)','(\S{5})','')))
        runL = np.asarray([rexProc.match(osp.split(dr)[-1]).groups()[0] if rexProc.match(osp.split(dr)[-1]) else '0' for dr in drL], dtype=int)

        self.lastRunProc = runL.max() if len(runL)!=0 else self.firstRun
        print("\nGot last Processed run: {0}\nFrom globbing: {1}".format(self.lastRunProc, fnDir))

        return

    def getAvailableProcRuns(self):
        """
        Get Last Processed Run from globbing the processed folder
        """
        fnDir = osp.join(self.path.pathProc, '*')
        drL  = glob.glob(fnDir)
        #check available run

        rexProc = re.compile(r'{0}'.format(self.path.drStrpattern%('(\d+)','(\S{5})','')))
        runL = np.asarray([rexProc.match(osp.split(dr)[-1]).groups()[0] if rexProc.match(osp.split(dr)[-1]) else '0' for dr in drL], dtype=int)
        runL = np.unique(np.sort(runL[runL>=self.firstRun]))
        self.availableRunsProc = runL

        print("\nFound available Processed runs {0}--{1} from globbing: {2}".format(self.availableRunsProc.min(), self.availableRunsProc.max(), fnDir))

        return


    def getEventList(self):
        
        runNo = []
        runL  = []
        for run in self.availableRunsProc:
            #runDr = osp.join(self.path.pathProc, self.path.drStrpattern%(run,self.path.dirDescriptors[0],''))
            runDr = osp.join(self.path.pathProc, self.path.pathPart)
            runFn = osp.join(runDr, self.path.fnLEpattern%run)
            if not osp.isfile(runFn):
                print("No event list for run {0}: {1}".format(run, runFn))
                runNo.append(run)
            else:
                print("Eventlist for run {0}: {1}".format(run, runFn))
                runL.append(run)
        
        self.availableRunsEvents = np.asarray(runL)
        self.noRunEvent         = np.asarray(runNo)

        return

    def getLastEvent(self):

        fnDir  = osp.join(self.path.pathPart, self.path.fnLEpattern%'*')
        lstL   = glob.glob(fnDir)
        rexLst = re.compile(r'%s'%self.path.fnLEpattern%'(\d+)')
        runL   = np.asarray([rexLst.match(osp.split(lstfn)[1]).groups()[0] if rexLst.match(osp.split(lstfn)[1]) else '0' for lstfn in lstL], dtype=int)

        self.availableRunsEvent = np.unique(np.sort(runL)) 
        self.lastRunEvent = runL.max() if len(runL)!=0 else self.firstRun
        print("\nGot last Event run: {0}\nFrom globbing: {1}".format(self.lastRunEvent, fnDir))
        print("\nFound available Processed runs {0}--{1} from globbing: {2}".format(self.availableRunsEvent.min(), self.availableRunsEvent.max(), fnDir))
        
        return


    def makeEventList(self, recreate=False):

        runList = self.availableRunsProc if recreate else self.noRunEvent

        for run in runList:
            _d = self.path.dirDescriptors[0]

            prcDr  = osp.join(self.path.pathProc, self.path.drStrpattern%(run,_d,''))
            prcFn  = osp.join(prcDr, self.path.fnStrpattern%('run'+str(run),_d,'','h5'))

            lstDr  = osp.join(self.path.pathProc, self.path.pathPart)
            lstFn  = osp.join(lstDr, self.path.fnLEpattern%run)
            #bork

            geomFn = osp.join(self.path.pathConf, self.path.geomFn)

            tmpFn = osp.join(lstDr,'_tmp{0}-{1}.lst'.format(run,_d))
            gen_lst_tmp(tmpFn, prcFn)

            call = [str(self.path.listEvents), '-i', tmpFn, '-o', lstFn, '-g', geomFn]
            p = sbp.run(call, stdin=sbp.PIPE, stdout=sbp.PIPE, stderr=sbp.PIPE, universal_newlines=True)

            if p.returncode!=0:
                print("This call did not work: {0}".format(' '.join(p.args)))
                print("Error: {0}".format(' '.join(p.stderr)))
                return
            else:
                print("Run {0} created event list: {1}".format(run,lstFn))

        #self.currentRun = int(p.stdout)
        #print("Retrieved current run: {0}".format(self.currentRun))

        return

    def getLastMopoRun(self):
        fnDir  = osp.join(self.path.pathMopo, self.path.fnMopPattern%'*')
        mopL   = glob.glob(fnDir)
        rexMop = re.compile(r'%s'%self.path.fnMopPattern%'(\d+)')
        runL   = np.asarray([rexMop.match(osp.split(mopfn)[1]).groups()[0] if rexMop.match(osp.split(mopfn)[1]) else '0' for mopfn in mopL], dtype=int)
        #runL.sort()

        self.availableRunsMopo = np.unique(np.sort(runL)) 
        self.lastRunMopo = runL.max() if len(runL)!=0 else self.firstRun
        print("\nGot last Mopo run: {0}\nFrom globbing: {1}".format(self.lastRunMopo, fnDir))

        if runL.size!=0:
            self.availableRunsMopo = np.unique(np.sort(runL))
            print("\nFound available Mopo runs {0}--{1} from globbing: {2}".format(self.availableRunsMopo.min(), self.availableRunsMopo.max(), fnDir))
        else:
            self.availableRunsMopo = np.asarray([0])
            print("No Mopo runs files found in {0}".format(fnDir))

        return

    #def genDummyTTcsv(self, tagL):
        ##import scipy as sp

        #fn = osp.join(self.path.pathTT,self.path.fnCsvPattern%self.runNum)

        #tagL = np.asarray(tagL)

        #numShot = tagL.size

        #dist_f     = np.random.normal(loc=800, scale=250, size=numShot).astype(np.float)
        #dist_d     = np.random.normal(loc=800, scale=250, size=numShot).astype(np.float)
        ##numNanf    = np.random.randint(0.,dist_f.size)
        ##numNand    = np.random.randint(0.,dist_d.size)
        #dist_d_nan = np.random.normal(loc=800, scale=250, size=dist_d.size).astype(np.int)
        #dist_f_nan = np.random.normal(loc=800, scale=250, size=dist_f.size).astype(np.int)

        #dist_d[dist_d_nan>800./np.random.randint(1.,2.)] = np.nan
        #dist_f[dist_f_nan>800./np.random.randint(1.,2.)] = np.nan

        #dummy = np.hstack((tagL,dist_d,dist_f), dtyle=object)
        #np.savetxt(fn, 

        #return

    def getLastTTRun(self):
        """
        Gets last processed TT run number
        """
        fnDir = osp.join(self.path.pathTT,self.path.fnCsvPattern%'*')

        TTL    = glob.glob(fnDir)
        rexTT  = re.compile(r'%s'%self.path.fnCsvPattern%'(\d+)')
        runTT  = np.asarray([rexTT.match(osp.split(TTfn)[1]).groups()[0] for TTfn in TTL], dtype=int)

        
        if runTT.size!=0:
            self.availableRunsTT = np.unique(np.sort(runTT))
            print("\nFound available TT runs {0}--{1} from globbing: {2}".format(self.availableRunsTT.min(), self.availableRunsTT.max(), fnDir))
        else:
            self.availableRunsTT = np.asarray(self.firstRun)
            print("No TT runs files found in {0}".format(fnDir))
            #print("Defaulting to available processed runs: {0}".format(self.availableRunsProc))
            #self.availableRunsTT = self.availableRunsTT

        self.lastRunTT       = runTT.max() if len(runTT)!=0 else self.firstRun
        print("\nGot last TT run: {0}\nFrom globbing: {1}".format(self.lastRunTT, fnDir))

        return

    #def getAvailableTTRuns(self):
        #"""
        #Get Last TT Run from globbing the processed folder
        #"""
        #fnDir = osp.join(self.path.pathTT, '*')
        #drL  = glob.glob(fnDir)
        ##check available run

        #rexProc = re.compile(r'{0}'.format(self.path.drStrpattern%('(\d+)','(\S{5})','')))
        #runL = np.asarray([rexProc.match(osp.split(dr)[-1]).groups()[0] if rexProc.match(osp.split(dr)[-1]) else '0' for dr in drL], dtype=int)
        #self.availableRunsProc = np.unique(np.sort(runL[runL!=0]))
        ##bork
        #print("\nFound available Processed runs {0}--{1} from globbing: {2}".format(self.availableRunsProc.min(), self.availableRunsProc.max(), fnDir))

        #return


    def getLastTagsRun(self):
        """
        Gets the last run from the TT folder csv\'s
        """
        fnDir  = osp.join(self.path.pathTag, self.path.fnTagPattern%'*')

        tagL   = glob.glob(fnDir)
        rexTag = re.compile(r'%s'%self.path.fnTagPattern%'(\d+)')
        runL   = np.asarray([rexTag.match(osp.split(tagfn)[1]).groups()[0] for tagfn in tagL], dtype=int)

        self.lastRunTags = runL.max() if len(runL)!=0 else self.firstRun

        print("\nGot last Tags run: {0}\nFrom globbing: {1}".format(self.lastRunTags, fnDir))

        return

    def getLastTagsRunCheetah(self):
        """
        Used instead of self.getLastTagsRun() if you want to avoid having to generate tag lists
        with MakeTagList.
        Reads the tags from the processed folder self.path.pathProc.
        """

        drL  = glob.glob(osp.join(self.path.pathProc, '*'))
        rexL = []
        runL = []
        for d in self.path.dirDescriptors:
            rex = re.compile(r'{0}'.format(self.path.drStrpattern%('(\d+)','{0}'.format(d),'')))
            rexL.append([rex])
            run = np.asarray([rex.match(osp.split(dr)[-1]).groups()[0] if rex.match(osp.split(dr)[-1]) else '-1' for dr in drL], dtype=int)
            runL.append(run)
        
        runL = np.unique(runL).ravel()
        found = np.sort(runL[runL!=-1])

        if found.size==0:
            raise ValueError("No runs found in processed folder! {0}".format(self.path.pathProc))

        self.procRuns    = found
        self.lastRunTags = self.procRuns.max()
        return

    def writeLastProcTagMopo(self):
        """
        Writes the file self.path.lastMopoTagfn
        """
        fn   = osp.join(self.path.pathMeta, self.path.lastMopoTagfn)
        hdr  = '# {0}, {1}, {2}, {3}\n'.format('Last processed run', 'TT', 'tags', 'mopo')
        runs = '{0} {1} {2} {3}'.format(self.lastRunProc, self.lastRunTT, self.lastRunTags, self.lastRunMopo)

        with open(fn, 'w') as fp:
            fp.write(hdr)
            fp.write(runs)
        print('Written {0} file:\nLast processed run, TT, tags, and mopo\n{1}, {2}, {3}, {4}'.format(fn, self.lastRunProc, self.lastRunTT, self.lastRunTags, self.lastRunMopo))

        return

    def readLastProcTagMopo(self):
        """
        Reads the file self.path.lastMopoTagfn
        """

        fn = osp.join(self.path.pathMeta, self.path.lastMopoTagfn)
        rawRun = np.genfromtxt(fn, delimiter='', dtype=int)

        self.lastRunProc, self.lastRunTT , self.lastRunTags, self.lastRunMopo = rawRun[0], rawRun[1], rawRun[2], rawRun[3]

        print("Successfully read last processed run, TT, tags, and mopo:\n{0}, {1}, {2}, {3}".format(self.lastRunProc, self.lastRunTT, self.lastRunTags , self.lastRunMopo))

        return



    def getMissingTags(self):

        missingRuns = np.sort([self.lastRunMopo,self.lastRunTags])

        mRL  = np.arange(missingRuns.min(), missingRuns.max()+1, 1, dtype=int)

        self.Tags = {}

        tags = []
        runL = []
        for run in mRL:
            try:
                #print(run)
                raw = readTagFile(osp.join(self.path.pathTag, self.path.fnTagPattern%run))
                self.Tags[run] = {'tags':raw, 'Ntags':raw.size}
                tags+=raw.tolist()
                runL+=[run]
            except OSError:
                print("Could not find {0}".format(osp.join(self.path.pathTag, self.path.fnTagPattern%run)))

        self.Tags['runs'] = runL

        self.missingTags = np.asarray(tags, dtype=int)

        return

    def getMissingTagsCheetah(self):

        if not hasattr(self, 'lastRunTags'):
            self.getLastTagsRunCheetah()

        if not hasattr(self, 'lastRunMopo'):
            self.getLastMopoRun()

        missingRuns = np.sort([self.lastRunMopo,self.lastRunTags])
        mRL  = np.arange(missingRuns.min(), missingRuns.max()+1, 1, dtype=int)

        self.noTags   = []
        self.Tags = {}

        tags = []
        runL = []

        for run in self.procRuns:
            self.Tags[run] = {}
            for d in self.path.dirDescriptors:
                fn = osp.join(self.path.pathProc, self.path.drStrpattern%(run,'{0}'.format(d),''), self.path.fnStrpattern%(run,'{0}'.format(d),'','csv'))
                rawCht = extractCheetahCSV(fn, run)
                if type(rawCht)!=type(None):
                    print(run,fn)
                    (h5, hit, _tags) = rawCht
                    #bork
                    self.Tags[run][d]= {'tags':_tags, 'Ntags':_tags.size, 'isHit':hit}
                    tags+=_tags[hit].tolist()
                    runL+=[run]
                else:
                    self.noTags.append(run)

        self.Tags['runs'] = runL
        self.missingTags = np.asarray(tags, dtype=int)

        self.writeMissingTags()

        return


    def writeMissingTags(self):
        """
        Write missing tags to file, so that the dbrequest script can get them.
        """
        fn = osp.join(self.path.pathTag, self.path.tagReqfn)
        np.savetxt(fn, self.missingTags, fmt='%.0f')
        print("Written missing tags in: {0}".format(fn))
        return


    def genDummyMopo(self):
        """
        Only for testing purposes
        """

        self.nomTmopo = np.ones_like(self.missingTags)*4120
        self.TTmopo   = np.ones_like(self.missingTags)*12970
        self.LaserOn  = np.ones_like(self.missingTags)

        return

    def sendRequestMopo(self, dummy=False):
        """
        Adds mopo request to queue via the script in the self.path.bRequestScript attribute.
        """

        import dbpy
        script   = osp.join(self.path.pathScripts, self.path.dbRequestScript)
        outfn    = osp.join(self.path.pathTag, self.path.dbMopofn)
        t_h      = str(self.path.trigger_hi)
        motors   = ','.join(self.path.motors)
        tagsStr  = osp.join(self.path.pathTag, self.path.tagReqfn) # ','.join(self.missingTags.astype(str))
        pyVer    = self.path.pyVer
        qsubCall = self.path.qsubSubmit.split(',')

        call      = qsubCall + [pyVer, script, '-o', outfn, '-h', t_h, '-m', motors, '-t', tagsStr]
        dummyCall = [pyVer, script, '-o', outfn, '-s', t_h, '-m', motors, '-t', tagsStr]

        if not dummy:
            p = sbp.run(call, stdin=sbp.PIPE, stdout=sbp.PIPE, stderr=sbp.PIPE, universal_newlines=True)
        else:
            p = sbp.run(dummyCall, stdin=sbp.PIPE, stdout=sbp.PIPE, stderr=sbp.PIPE, universal_newlines=True)

        #p = sbp.run(('ShowNewestRunNumber', '-b', '2'),  stdin=sbp.PIPE, stdout=sbp.PIPE, stderr=sbp.PIPE, universal_newlines=True)

        if p.returncode!=0:
            print("This call did not work: {0}".format(' '.join(p.args)))
            return p

        #self.currentRun = int(p.stdout)
        #print("Requested mopos for current run: {0}".format(self.currentRun))

        #if TTmopo.size!=self.tags.size:
            #raise ValueError("Something went wrong with the database request!")
        return p


    def readRequestedMopo(self):

        fn   = osp.join(self.path.pathTag, self.path.dbMopofn)
        raw  = np.genfromtxt(fn, dtype=int)
        sIdx = np.argsort(raw[:,0])
        rawS = raw[sIdx,:]
        # Assumes the order is nomTmopo TTmopo isLaser. As written in the ini file.

        self.reqTags  = np.asarray(rawS[:,0], dtype=int)
        self.nomTmopo = np.asarray(rawS[:,1], dtype=int)
        self.TTmopo   = np.asarray(rawS[:,2], dtype=int)
        self.isLaser  = np.asarray(rawS[:,3], dtype=int)

        if not np.all(np.unique(self.reqTags).sort() == np.unique(self.missingTags).sort()):
            raise ValueError("self.reqTags and self.missingTags do not match!")

        return

    def requestMopoFromRun(self, force=None, overwrite=False, from_first=False):
        """
        Only works at SACLA on a qsub -I shell!!!! (with python3.5)
        Requests tags, nominal time mopo, TT mopo, and laser shutter is on from database
        on a per run basis.
        """
        import dbpy

        if type(force)!=type(None):
            missingRuns = np.atleast_1d(force)
        elif from_first:
            missingRuns = np.sort([self.currentRun, self.firstRun]) 
        else:
            missingRuns = np.sort([self.lastRunMopo, self.currentRun]) if self.currentRun!=self.lastRunMopo else np.asarray([self.lastRunMopo])

        self.missingRuns = np.arange(missingRuns.min(), missingRuns.max(), 1) if missingRuns.size!=1 else missingRuns
    
        self.Tags = {}
        self.Tags['runL'] = []
        sleep=False

        print("\nRuns to retrieve: {0}".format(' '.join(self.missingRuns.astype(str))))
        for run in self.missingRuns:

            tagFn = osp.join(self.path.pathTag, self.path.fnTagPattern%run)
            mopoFn = osp.join(self.path.pathMopo, self.path.fnMopPattern%run)
            
            #print("requestMopoFromRun: "+str(run)+str(run in self.badRuns))
            
            if run in self.badRuns and not overwrite:
                print("requestMopoFromRun: run {0} is bad not requesting mopo info.".format(run))
                continue
            else:
                if (osp.isfile(mopoFn) and osp.isfile(tagFn)) and not overwrite:
                    print("Run {0} already processed for Tags and Mopo.".format(run))
                    continue
                else:

                    print("Requesting tags and mopo\'s for run: {0}".format(run))
                    try:
                        if dbpy.read_runstatus(self.path.beamline, run)==0:
                            trigger_hi = int(dbpy.read_hightagnumber(self.path.beamline, run))
                            tags     = dbpy.read_taglist_byrun(self.path.beamline, run)
                            nomTmopo = np.asarray(dbpy.read_syncdatalist_float(self.path.nomTmotor, trigger_hi, tags), dtype=int)
                            TTmopo   = np.asarray(dbpy.read_syncdatalist_float(self.path.TTmotor, trigger_hi, tags), dtype=int)
                            NDmopo   = np.asarray(dbpy.read_syncdatalist_float(self.path.NDmotor, trigger_hi, tags), dtype=int)
                            LaserOn  = np.asarray(dbpy.read_syncdatalist_float(self.path.laserON, trigger_hi, tags), dtype=int)
                            LaserOff = np.asarray(dbpy.read_syncdatalist_float(self.path.laserOFF, trigger_hi, tags), dtype=int)

                            self.Tags[run]  = {'tags'  : tags,
                                                'nomT' : nomTmopo,
                                                'TT'   : TTmopo,
                                                'ND'   : NDmopo,
                                                'isLas': LaserOn,
                                                'noLas': LaserOff,
                                                'isHit': np.ones_like(tags),
                                                'Ntags': len(tags)}
                            self.Tags['runL'].append(run)

                            print("Successfully received tags and mopo for run: {0}\n".format(run))

                            sleep=False
                        else:
                            sleep=True
                            print("Run {0} not ready yet.".format(run))

                    except dbpy.APIError:

                        print("Run {0} could not request mopos from db.".format(run))

                        sleep=True

                        continue

                self.lastRunMopo = run
                self.lastRunTags = run

        return sleep


    def writeTagsMopoPerRun(self,overwrite=False):
        """
        Works for the getMopoFromRun()
        writes a tag list file, like the MakeTagList output.
        Writes a tag mopo file.
        """

        for run in self.Tags['runL']:
            tagFn = osp.join(self.path.pathTag, self.path.fnTagPattern%run)
            mopoFn = osp.join(self.path.pathMopo, self.path.fnMopPattern%run)

            #print("writeTagsMopoPerRun: "+str(run)+str(run in self.badRuns))
            #print("Bad runs: ", self.badRuns)

            if run in self.badRuns and not overwrite:
                print("writeTagsMopoPerRun: run {0} is bad not writing tags and mopo file.".format(run))
                #bork
                continue
            else:
                if (osp.isfile(mopoFn) and osp.isfile(tagFn)) and not overwrite:
                    print("writeTagsMopoPerRun: run {0} already processed for Tags and Mopo.".format(run))
                    continue
                else:

                    try:
                        out = np.vstack((np.asarray(self.Tags[run]['tags']), self.Tags[run]['nomT'], 
                                         self.Tags[run]['TT'], self.Tags[run]['ND'], self.Tags[run]['isLas'],
                                         self.Tags[run]['noLas'])).T
                    except ValueError:
                        print("Run {0} has a shape mismatch!!! Recording as bad run.".format(run))
                        if False:
                            print("Tags:{0}\nnomT:{1}\nTT:{2}\nND:{3}\nisLas:{4}\nnoLas:{5}\n".format(len(self.Tags[run]['tags']),
                                                                    self.Tags[run]['nomT'].shape,
                                                                    self.Tags[run]['TT'].shape,
                                                                    self.Tags[run]['ND'].shape,
                                                                    self.Tags[run]['isLas'].shape,
                                                                    self.Tags[run]['noLas'].shape))

                        self.recordBadRun(run, 'Shape mismatch between tags and mopos.')

                        return

                    #print(out.shape)
                    #sys.exit(0)

                    outHdr = ' {0}, {1}, {2}, {3}, {4}, {5}'.format('tag','nomTmotor','TTmotor','NDmotor','LaserON', 'laserOFF')
                    np.savetxt(mopoFn, out, fmt='%.0f', header=outHdr, comments='#')

                    tagHdr = ' Tags for run {0}'.format(run)
                    np.savetxt(tagFn, np.asarray(self.Tags[run]['tags'], dtype=int), fmt='%.0f', header=tagHdr, comments='#')

                    print(("\nWritten for run {0}:\n"+
                        "Mopo\'s : {1}\n"+
                        "Tags   : {2}\n").format(run, mopoFn, tagFn))

        return

    def requestTMAoffline(self, force=None, reanalyze=False):
        """
        Here force is a range
        """

        if type(force)!=type(None):
            self.missingTTruns = np.asarray(force)
        elif reanalyze:
            self.missingTTruns = self.availableRunsTT
        else:
            self.missingTTruns = np.setdiff1d(self.availableRunsProc, self.availableRunsTT) # Order of input arrays matters!!!
            #self.missingTTruns = np.asarray([self.availableRunsTT.max(), self.lastRunProc])
        print("testTMA: {0}".format(self.missingTTruns))
        #sys.exit(0)
        prg   = self.path.ttMonAnalyzer
        conf  = osp.join(self.path.pathConf, self.path.confTTfn)
        minR  = str(self.missingTTruns.min())
        maxR  = str(self.missingTTruns.max())
        call  = [prg, conf, minR, maxR]
        
        print("Running {0} with the following call: {1}".format(prg, ' '.join(call)))
        
        print("Sent the following call: {0}".format(' '.join(call)))
        print("This may take a while... please be patient.")
        
        p = sbp.run(call, stdin=sbp.PIPE, stdout=sbp.PIPE, stderr=sbp.PIPE, universal_newlines=True)

        if p.returncode!=0:
            print("This call did not work: {0}".format(' '.join(p.args)))
            print("Error: {0}".format(' '.join(p.stderr)))
            return
        else:
            #print("TMAoffline said: {0}".format(' '.join(*p.stdout)))
            fnTMA = osp.join(self.path.pathTTjobs, self.path.fnTMApattern%((minR,maxR)))
            write_TMAoffline(fnTMA, p.stdout)
            print("Succesfully created TT data for runs {0}--{1}.".format(minR,maxR))

        return

    def writeTagsMopo(self, overWrite=False):
        """
        Seems to work only with the self.getMissingTags() method.
        """
        hdr = ' {0}, {1}, {2}, {3}'.format('tag','nomTmotor','TTmotor','isLaser')
        Ntags=0

        for run in self.Tags['runs']:
            idxT  = self.Tags[run]['Ntags']
            tags  = self.missingTags[Ntags:Ntags+idxT]
            outFn = osp.join(self.path.pathTag, self.path.fnMopPattern%run)

            if osp.isfile(outFn) and not overWrite:
                print("Tag mopo file already exists, not overwriting: {0}".format(outFn))
                pass
            else:
                if not np.all(tags == self.Tags[run]['tags']):
                    raise ValueError("FAIL!")
                else:
                    self.Tags[run]['nomT']  = self.nomTmopo[Ntags:Ntags+idxT]
                    self.Tags[run]['TT']    = self.TTmopo[Ntags:Ntags+idxT]
                    self.Tags[run]['isLas'] = self.isLaser[Ntags:Ntags+idxT]
                    out   = np.vstack((self.Tags[run]['tags'],self.Tags[run]['nomT'],self.Tags[run]['TT'],self.Tags[run]['isLas'])).T

                    np.savetxt(outFn, out, fmt='%.0f', header=hdr, comments='#')
                    print("Written tag mopo file: {0}".format(outFn))

                Ntags += self.Tags[run]['Ntags']

        return

    def writeTagsMopoRobust(self, overWrite=False):
        hdr = ' {0}, {1}, {2}, {3}'.format('tag','nomTmotor','TTmotor','isLaser')
        Ntags=0

        for run in self.Tags['runs']:

            #idxT  = self.Tags[run]['Ntags']
            for d in self.path.dirDescriptors:
                add = str(run)+'-'+d
                outFn = osp.join(self.path.pathTag, self.path.fnMopPattern%add)

                try:
                    tags  = self.Tags[run][d]['tags']
                    isHit = self.Tags[run][d]['isHit']
                    idxT  = self.reqTags.searchsorted(tags[isHit])
                    
                    if osp.isfile(outFn) and not overWrite:
                        print("Tag mopo file already exists, not overwriting: {0}".format(outFn))
                        pass

                    else:
                        self.Tags[run][d]['nomT']  = self.nomTmopo[idxT]
                        self.Tags[run][d]['TT']    = self.TTmopo[idxT]
                        self.Tags[run][d]['isLas'] = self.isLaser[idxT]

                        out   = np.vstack((self.Tags[run][d]['tags'][isHit],self.Tags[run][d]['nomT'],self.Tags[run][d]['TT'],self.Tags[run][d]['isLas'])).T
                        if out.size!=0:
                            np.savetxt(outFn, out, fmt='%.0f', header=hdr, comments='#')
                            print("Written tag mopo file: {0}".format(outFn))
                        else:
                            print("run {0} has no hits!".format(add))

                except KeyError:
                    print("run {0} has no tags!".format(add))
                    
                    pass

                #Ntags += self.Tags[run]['Ntags']

        return

class ParTT:
    """
    Partialate class.
    """
    def __init__(self, pthObj):
        """
        """
        #bork
        if not isinstance(pthObj, PathTT):
            raise TypeError("")
        else:
            self.path = pthObj

        if not np.isclose(self.path.mopo2fs, 6.666666666667, rtol=1e-3):
            raise Warning("kwarg mopo2fs motor position to femtosecond conversion is off from 6.666666666667.")

        self.getMetaData()

        return

    def loadSampleList(self):
        fn = osp.join(self.path.pathMeta, self.path.sampleListfn)
        rawSamp = np.atleast_2d(np.genfromtxt(fn, dtype=object))
        rawSamp[:,:2] = rawSamp[:,:2].astype(int)
        rawSamp[:,2] = rawSamp[:,2].astype(str)
        
        self.sampleList = rawSamp
        print("Loaded sample list: {0}".format(fn))
        return

    def getMetaData(self):
        """
        """

        self.meta = MetaTT(self.path)
        self.meta.readCurrentRun()

        self.meta.getLastMopoRun()
        self.meta.getLastTTRun()
        self.meta.getLastEvent()
        self.getAvailableMopoTTRuns()

        return

    def getAvailableMopoTTRuns(self):

        fnDir = osp.join(self.path.pathMopo,self.path.fnTTMPattern%'*')

        MTTL    = glob.glob(fnDir)
        rexMTT  = re.compile(r'%s'%self.path.fnTTMPattern%'(\d+)')
        runMTT  = np.asarray([rexMTT.match(osp.split(MTTfn)[1]).groups()[0] for MTTfn in MTTL], dtype=int)

        self.lastRunMopoTT           = runMTT.max() if len(runMTT)!=0 else self.meta.firstRun
        print("\nGot last MopoTT run: {0}\nFrom globbing: {1}".format(self.lastRunMopoTT, fnDir))

        if runMTT.size!=0:
            self.availableRunsMopoTT = np.unique(np.sort(runMTT))
            print("\nFound available MopoTT runs {0}--{1} from globbing: {2}".format(self.availableRunsMopoTT.min(), self.availableRunsMopoTT.max(), fnDir))
        else:
            self.availableRunsMopoTT = np.asarray([0])
            print("No MopoTT runs files found in {0}".format(fnDir))


        return

    def pairRunEventTT(self, force=None, plot=False):

        #lstDr  = osp.abspath(self.path.pathPart)
        #lstFn  = osp.abspath(osp.join(lstDr, self.path.fnLEpattern%run))
        #self.meta.avai

        #mopDr    = osp.abspath(self.path.pathMopo)
        #mopFnPat = osp.join(mopDr)

        #TTDr     = osp.abspath(self.path.pathTT)
        #TTFnPat  = osp.join(TTDr, self.path.fnCsvPattern)


        if type(force)!=type(None):
            runs2MopoTT = np.asarray(force)
        else:
            self.sampleList
            runs2MopoTT = np.intersect1d(np.intersect1d(self.meta.availableRunsMopo,self.meta.availableRunsTT), self.meta.availableRunsEvent)

        #bork

        self.MopoTT = {}

        for run in runs2MopoTT:
            S = {}
            
            S['Efn'], S['Etags'] = self.loadEventList(run)
            if type(S['Etags'])!=type(None):
                S['TT'] = self.loadTT(run, plot=plot)

                #np.all(np.diff(S['TT'].tags)>0)
                #np.all(np.diff(S['Etags']) >0)
                
                idxTags = S['TT'].tags.searchsorted(S['Etags'])
                idxTags[idxTags==S['TT'].tags.size]=-1
                
                if not np.all(S['Etags'] == S['TT'].tags[idxTags]):
                    mask = (S['Etags'] == S['TT'].tags[idxTags])
                    S['Etags'] = S['Etags'][mask]
                    #S['TT'].tags = S['TT'].tags[mask]
                    print("Offending tag(s) removed: {0}".format(S['TT'].tags[idxTags][~mask]))
                    idxTags = S['TT'].tags.searchsorted(S['Etags'])
                    
                    #raise ValueError("Dafuq?")

                S['t']     = S['TT'].t_a[idxTags]
                S['xtra']  = S['TT']._mopoXtra[S['TT']._idxMopoTags][idxTags]
                
                idxNan = np.isnan(S['t'])
                _bad = S['Etags'][idxNan]
                S['t']     = S['t'][~idxNan]
                S['xtra']  = S['xtra'][~idxNan]
                S['Etags'] = S['Etags'][~idxNan]
                
                if _bad.size!=0:
                    print("Removed Nan times from tags: {0}".format(_bad))

                self.MopoTT[run] = S
                #S['Etags']
                #self.writeTagTime(run, S)

            else:
                print("Run {0} processed file {1} contains no tags!".format(run, S['Efn']))
                continue

        return

    def writeTagTime(self, TT, overwrite=False):
        run = TT.runNum
        dr = osp.abspath(self.path.pathMopo)
        fn = osp.join(dr, self.path.fnTTMPattern%run)
        hdr = '# Run {0}\n# Tags time\n'.format(run)
        out = np.vstack((TT.tags,TT.t_a)).T
        if osp.isfile(fn) and not overwrite:
            print("Run {0} Tag time file already exists, skipping: {1}".format(run, fn))
        else:
            with open(fn, 'w') as fp:
                fp.write(hdr)
                for line in out:
                    fp.write('{0:.0f} {1:.2f}\n'.format(*line))
            print("Run {0} written Tag time file: {1}".format(run, fn))

        return


    def binRunEventTT(self, sample=None, bins=None, resolution=100):

        if type(sample)!=type(None):
            borm
        else:
            self.loadSampleList()

        self.Sample={}
        

        for i,sample in enumerate(self.sampleList[:,2]):
            lims = self.sampleList[i,:2]
            sampleRun = np.arange(lims.min(), lims.max())
            print("Binning sample {0} runs: {1}--{2}".format(sample, sampleRun.min(), sampleRun.max()))
            #_D = {}
            #self.Sample[sample] = _D.fromkeys(bins)

            self.Sample[sample] = {'raw':[],'Efn':[]}
            for run in sampleRun:
                if run in self.MopoTT:
                    self.Sample[sample]['raw'].append(np.vstack((self.MopoTT[run]['Etags'].astype(float), self.MopoTT[run]['t'].astype(float))).T)
                    self.Sample[sample]['Efn'].append(np.tile(self.MopoTT[run]['Efn'], (self.MopoTT[run]['Etags'].shape[0])))
                else:
                    print("Run {0} does not have any tagged shots.".format(run))

            self.Sample[sample]['raw']      = np.vstack(self.Sample[sample]['raw']).astype(object)
            self.Sample[sample]['raw'][:,0] = self.Sample[sample]['raw'][:,0].astype(int)

            self.Sample[sample]['Efn'] = np.hstack(self.Sample[sample]['Efn'])

            if self.Sample[sample]['Efn'].shape[0] != self.Sample[sample]['raw'].shape[0]:
                raise ValueError("Shapes of Event")

            if type(bins)!=type(None):
                bins = np.sort(bins)
            else:
                bins = self.smartBinner(self.Sample[sample]['raw'][:,1], resolution=resolution)

            idxBin = bins.searchsorted(self.Sample[sample]['raw'][:,1])
            for i,_bin in enumerate(bins):
                mask = idxBin == i
                if np.any(mask):
                    self.Sample[sample][_bin]={'data':self.Sample[sample]['raw'][mask,:],
                                            'fn'  :self.Sample[sample]['Efn'][mask]}
                else:
                    print("No tags to bin in : {0}".format(_bin))

        return self.Sample

    def writePairedRunEventTT(self, prefix='', postfix='', overwrite=False):
        """
        """

        #TODO: WRITE FILE!!!
        for sam in self.Sample:
            for _bin in self.Sample[sam]:
                if _bin in ['raw', 'Efn']:
                    #bork
                    continue
                else:
                    dr = osp.abspath(osp.join(self.path.pathPart, sam))
                    fn = osp.join(dr, self.path.fnPartpattern%(prefix+str(sam),'{0:.0f}{1}'.format(_bin,postfix)))

                    if not osp.isdir(dr):
                        os.mkdir(dr)
                    
                    out = np.vstack((self.Sample[sam]['Efn'],self.Sample[sam]['raw'][:,0])).T
                    if not osp.isfile(fn) or overwrite:
                        with open(fn, 'w') as fp:
                            for line in out:
                                fp.write('{0} {1}\n'.format(*line))
                        print("Time bin {0} written in {1}".format(_bin,fn))
                    else:
                        raw = np.genfromtxt(fn)
                        test = raw[:,1]
                        with open(fn, 'a') as fp:
                            for line in out:
                                if not line[1] in test:
                                    fp.write('{0} {1}\n'.format(*line))
                                    print("Time bin {0} processed file {1} appended to {3}".format(_bin,line[0],fn))
                                else:
                                    print("Time bin {0} processed file {1} already exists in {3}".format(_bin,line[0],fn))

        return
                


    def smartBinner(self, t, resolution):
        t = t.astype(float)
        t = t[~np.isnan(t)]
        span  = np.diff((t.min(),t.max()))
        nBins = int(span/resolution)
        bins = np.linspace(t.min(),t.max(), num=nBins, endpoint=True, dtype=int) if nBins!=1 else np.asarray([t.mean()], dtype=int)
        return bins

    def loadTT(self, run, plot=False, write=True, overwrite=False):
        """
        Loads the TT .csv file of the runs in self.runsToPart
        """
        pthObj = PathTT(self.path.iniFn)
        pthObj.loadZero()
        

        TT = RunTT(pthObj)
        TT.loadTT(run)
        TT.loadMopo()
        #TT.includeMissingTags()
        TT.getCalibrationFromFile()
        TT.timeFromCalibration()

        if plot:
            
            TT.plotDiagnostics()
            plt.show(block=False)
            
        if write:
            self.writeTagTime(TT, overwrite=overwrite)

        return TT

    def loadEventList(self, run):

        lstDr = osp.abspath(self.path.pathPart)
        lstFn = osp.join(lstDr, self.path.fnLEpattern%run)
        
        _d = self.path.dirDescriptors[0]
        prcDr  = osp.join(self.path.pathProc, self.path.drStrpattern%(run,_d,''))
        #prcRe  = osp.join(prcDr, self.path.fnStrpattern%('run'+'(\d+)',_d,'','h5'))
        prcRe  = self.path.fnStrpattern%('run'+'(\d+)',_d,'','h5')

        prcFn  = osp.join(prcDr, self.path.fnStrpattern%('run'+str(run),_d,'','h5'))

        tags = readEventsList(lstFn, run, prcRe)
        if tags.size!=0:
            return (prcFn,tags)
        else:
            return (prcFn, None)



    def writePartList(self):
        return
