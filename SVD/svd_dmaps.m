% close all; 
clear all;
sdir='/home/amke/libsfx/SVD/100uj-14pk/';

files=dir([sdir '*.map']);
%files(1:2)=[];
%% load all maps
sig_times=2.5; % set all values below sig_times*sigma to 0
for i=1:length(files)
    [a,EDmax,header,numelectrons,dx,dy,dz]=readMRCfile([sdir files(i).name]);
    %set all values that are below xsigma to zero
    [sigma]=std(a(:));
    %figure(111)
    %subplot(2,1,1)
    %hist(a(:),50);
    
    a(abs(a)<sig_times*sigma)=0;
%     a = a(3e6:end);
    %subplot(2,1,2)
    %hist(a(:),50);
    
    if i==1;
        EDall=zeros(length(a(:)),length(files));
        names=cell(length(i),1);
    end
    EDall(:,i)=a(:);
    
    names{i,1}=files(i).name;

    
    %figure out delay time
    ind=regexp(names{i},'fs')
    if strcmp(names{i}(1),'n')
        delaytime(i)=-str2double(names{i}(4:ind-1));
    else
        delaytime(i)=str2double(names{i}(1:ind-1));
    end
end
[dtime,I]= sort(delaytime);
EDalls=EDall(:,I);
filess=files(I);

%% svd part
[U,S,V]=svd(EDalls,'econ');
s=svd(EDalls,'econ');
for i=1:length(V)
    if mean(V(:,i))<0; V(:,i)=-V(:,i); U(:,i)=-U(:,i); end
end
%%
maxc=6;
figure;clf; 
for ii=1:maxc
subplot(2,1,1)
plot(ii,diag(S(ii,ii)),'x')
hold all
subplot(2,1,2)
plot(dtime,V(:,ii)*diag(S(ii,ii)),'x-')
hold all
end

%save the svd components
%rm([sdir '*ccp4'])
for iii=1:maxc
    ED_save=reshape(U(:,iii),size(a));
    fname=[sdir 'EDsvd' num2str(iii) '.ccp4']
    status= writeMRCfile(ED_save, fname,'float',[sdir files(1).name]);
    %test read this again
    %[b,bEDmax,bheader,bnumelectrons,bdx,bdy,bdz]=readMRCfile(fname);
end


%% reconstruct the time points
for iiii=1:maxc
    Us=U(:,1:maxc);
    Ss=S(1:maxc,1:maxc);
    Vs=V(:,1:maxc);
end
% Edmap_reco=Us*Ss*Vs';
% sdir_rec=[sdir 'svdrecon/'];
% if ~exist(sdir_rec); mkdir(sdir_rec); end
%     
% for i=1:length(filess)
%     fname=[sdir_rec filess(i).name];
%     EDmap_reco_save=reshape(Edmap_reco(:,i),size(a));
%     status= writeMRCfile(EDmap_reco_save, fname,'float',[sdir files(1).name]);
% end

%% PLot data and SVD
figure()
subplot(2,2,1)
plot(s,'x','LineWidth',3)

subplot(2,2,3)
plot(U(:,1), 'LineWidth',2)
hold on
plot(U(:,2), 'LineWidth',2)
% plot(q,U(:,3), 'LineWidth',2)
% plot(q,U(:,4), 'LineWidth',2)
legend('U_1','U_2')
xlabel('xyz-position')
set(gca, 'FontSize',16)
% xlim([0.5 4.5])

subplot(2,2,2)
plot(dtime,V(:,1)*s(1), 'LineWidth',2)
hold on
plot(dtime,V(:,2)*s(2), 'LineWidth',2)
plot(dtime,V(:,3)*s(3), 'LineWidth',2)
plot(dtime,V(:,4)*s(4), 'LineWidth',2)
xlabel('t / fs')
legend('V_1','V_2','V_3','V_4')
set(gca, 'FontSize',16)
% set(gca, 'XScale', 'log')
% xlim([-0.5 100])