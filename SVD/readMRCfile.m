function [a,EDmax,header,numelectrons,dx,dy,dz]= readMRCfile(fname,sfallmap)
% readMRCfile readMRCfile (fname)
% there is a dirty patch in this and the write routine - apparently the
% first 256'long' words are the header, but in header(24) there is a number
% of bytes given which describes how many bytes (typicallyy 80) are used to
% describe the symmetry after the header. 
%in matlab on linux the some words in the header are only corrected
%decoeded when using 'float' instead of 'long', have patched this
%number of electrons is the integral over all three dimensions of a

%SW2018
%[dx,dy,dz are the map dimensions in Å, but this is not tested]
if ~exist('sfallmap')
    sfallmap=false; %this flag switches the x and y dimensions when reading a map produced by sfall - apparently they are inverted. 
end


[fid,message]=fopen(fname,'r');
if fid == -1
    error('can''t open file');
    a= -1;
    return;
end
header=fread(fid,10,'long');
header=[header;fread(fid,6,'float')]; %for cell dimensions + angles
header=[header;fread(fid,3,'long')]; %for position x,y,z in col, row, sec
header=[header;fread(fid,3,'float')]; %for Amin, Amax, Amean
header=[header;fread(fid,234,'long')]; %for num bytes symm + rest
header=[header;fread(fid,header(24)/4,'long')]; %for num bytes symm + rest

nx=header(1);
ny=header(2);
nz=header(3);
type=header(4);
maxx=header(11);
EDmax=header(21);
fprintf(1,'nx= %d ny= %d nz= %d type= %d dim_x(Å)= %2.2f amean= %f \r', nx, ny,nz,type,header(11),header(22));

dx=maxx;
dy=maxx/nx*ny;
dz=maxx/nx*nz;


% Seek to start of data
%status=fseek(fid,1024+header(24),-1); %header(24) contains the number of bytes for symmetry operations
% Shorts
if type== 1
    a=fread(fid,nx*ny*nz,'int16');
end
%floats
if type == 2
    a=fread(fid,nx*ny*nz,'float32');
end
fclose( fid);

if sfallmap
    a= reshape(a, [nx ny nz]);
else
    a= reshape(a, [ny nx nz]);
end

%a=permute(a,[2 3 1]); %this line permutes the axes, so that electron denisty agrees with pdb when plottedn in MATLAB
% if nz == 1
%     a= reshape(a, [nx ny]);
% end

numelectrons=sum(a(:)*(header(11)/header(8))^3);