function status= writeMRCfile(a, fname,type,header_copy)

%head_copy - patch by SW to copy/paste the existing header of an ccp4 file
%for the new one. if header_name is not give a new header is geranted, but
%that misses lots of information.
%http://ami.scripps.edu/software/mrctools/mrc_specification.php


% dirty patch: this just copies the ehader + 80 bytes of symmetry
% operations from a reference map (header copy)
if exist('header_copy','var')
    [fid,message]=fopen(header_copy,'r');
    %header=fread(fid,10,'long');
    %header=[header;fread(fid,6,'float')]; %for cell dimensions + angles
    %header=[header;fread(fid,3,'long')]; %for position x,y,z in col, row, sec
    %header=[header;fread(fid,3,'float')]; %for Amin, Amax, Amean
    %header=[header;fread(fid,234,'long')]; %for num bytes symm + rest
    %header=[header;fread(fid,header(24)/4,'long')]; %for num bytes symm 
    header=fread(fid,256,'long');
    header=[header;fread(fid,header(24)/4,'long')]; %read extra num bytes symm 
    fclose(fid);
    % header(20)=min(a(:));
    % header(21)=max(a(:));
    % header(22)=mean(a(:));
    %header(24)=0; %set the num of bytes for symm operations (saved after 1024 bytes header to 0)
    
end
% type is float or short
[fid,message]=fopen(fname,'w');
if fid == -1
    error('can''t open file');
    a= -1;
    return;
end
ndims=size(size(a));
ndim=ndims(2);
dims(1:256)=1;
if ndims > 3
    ferror('too many dimensions');
end
b=size(a);
dims(1:ndim)=b(1:ndim);
if type=='float'
    dims(4)=2;
    itype='float32';
end
if type =='short'
    dims(4)=1;
    itype='uint16';
end
if ~exist('header')
    status=fwrite(fid,dims(1:256),'int32');
else
    status=fwrite(fid,header,'long')
end
status=fwrite(fid,a,itype);
fclose(fid);