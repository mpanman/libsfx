
clear all 
clc

%% Determine directory where to find files
path = '~/Dropbox/SACLA_TT_Team2021/run260621/';
path_out = '~/Dropbox/SACLA_TT_Team2021/out/';
path_power = '~/Dropbox/SACLA_TT_Team2021/power_corr/power_corr/';
start_run = 1009018; %1009469	1009502
stop_run = 1010047;
pixel2fs = 2.4;
centerpixel = 890;
t_zero = 0;%centerpixel*pixel2fs; 
mopo0corr = 87;%from '~/libsfx/metaData/zeroMopo.dat'
pls2fs = 6.6666666667;

runs = [start_run:stop_run];
tags = [];
TT_pixel = [];


%% Load files 
for i=1:length(runs)
    if isfile([path sprintf('%d',runs(i)) '.csv'])
        
        A=readtable([path sprintf('%d',runs(i)) '.csv']);
        tags = [tags; A.tag_number];
%         tag{i} = table2array(A(:,'tag_number'));
        TT_pixel = [TT_pixel; (A.timing_edge_derivative_pixel_+A.timing_edge_derivative_pixel_)/2];
        
    end
    
end

tags = tags(~isnan(TT_pixel));
TT_pixel = TT_pixel(~isnan(TT_pixel));

figure
histogram(TT_pixel)
xlabel('pixel number')


