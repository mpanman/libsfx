%% Make bins for time points and create files containing tag numbers for each bin

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% inputs needed: 
% csv files in 'path_delay' contain information on actual
% delay time and estimated power
% txt files in path_lst contain information on tag numbers of indexed
% patterns
% Start and stop run determine run numbers you are interested in (excluding
% e.g. YAG scan and TT calibration)

% input for create_bin.m:
% chose type of binning: 
% 'eqNpatterns': bin so that each bin contains the same number of patterns,
% number defined by N_patterns
% 'eqWidthBins': bin so that each bin has the same temporal width, width
% defined by width_timebin
% 'binEdges': define bin edges and bin according to these, edges defined by
% bin_edges

% Output 
% saved as csv files in 'path_out' (folder name containing
% information on binning method and specifics); 
% e.g. 650fs.csv created for each bin contating a list of corresponding tags
% properties.csv in output folder, includes mu, sigma, number of patterns
% and width for each created bin
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


clear all 
clc

addpath('/home/amke/libsfx/SACLA_TT_Team2021/time_binning')

%% Determine directory where to find files
path_delay = '~/libsfx/SACLA_TT_Team2021/out/';
path_lst = '~/libsfx/SACLA_TT_Team2021/';%'~/libsfx/SACLA_TT_Team2021/';%
path_output = '~/libsfx/SACLA_TT_Team2021/time_binning/out/';
start_run = 1009156; %1009469	1009502
stop_run = 1010047;
runs = [start_run:stop_run];
tags = [];
path = {};
delays = [];
power = [];
t = [];

%%
% l=1;
% for i=1:length(runs)  
%     if isfile([path_lst sprintf('%d_events.txt', runs(i))])
%         FID = fopen([path_lst sprintf('%d_events.txt', runs(i))]);
%         data = textscan(FID,'%s');
%         fclose(FID);
%         if ~size(data{1},1)/2==0
%             for ii=1:size(data{1},1)/2
%                 tag(ii) = str2double(data{1}{2*ii}(5:end-2));
%                 file{l} = (data{1}{2*ii});
%                 path{l} = (data{1}{2*ii-1});
%                 l=l+1;
%             end
%             A=readtable([path_delay sprintf('%d_delay',runs(i)) '.csv']);
%             tags = [tags tag];
%             delays = [delays; A.delay(ismember(A.tag_number, tag))];
%             power = [power; A.power_est(ismember(A.tag_number, tag))];
% 
%             clear tag
%         end
%     end
%     
% end
% FID = fopen([path_lst '20uj-clean.dat']);
FID = fopen([path_lst '100uj-clean.dat']);
% FID = fopen([path_lst '10uj-clean.dat']);
data = textscan(FID,'%s');
fclose(FID);
% if ~size(data{1},1)/2==0
%     for ii=1:round(size(data{1},1)/3)
%         tag(ii) = str2double(data{1}{3*ii-1}(5:end-2));
%         run(ii) = str2double(data{1}{3*ii-2}(34:40));
    for ii=1:round(size(data{1},1)/2)
        tag(ii) = str2double(data{1}{2*ii}(5:end-2));
        run(ii) = str2double(data{1}{2*ii-1}(29:35));
        file{ii} = (data{1}{2*ii});
        path{ii} = (data{1}{2*ii-1});
%         file{ii} = (data{1}{3*ii-1});
%         path{ii} = (data{1}{3*ii-2});
    end
    
    [tag_sort, idx] = sort(tag);
    run_sort = run(idx);
    file_sort = file(idx);
    path_sort = path(idx);

% end
 
runs = unique(run);
for i=1:length(runs)
    A=readtable([path_delay sprintf('%d_delay',runs(i)) '.csv']);
    tags = [tags; A.tag_number(ismember(A.tag_number, tag))];
    t = [t; A.delay(ismember(A.tag_number, tags))];
    power = [power; A.power_est(ismember(A.tag_number, tags))];
end
        
[tagsort,idxs] = sort(tags);
delays = t(idxs);
power = power(idxs);
path = path_sort(ismember(tag_sort,tagsort));
file = file_sort(ismember(tag_sort,tagsort));

tags = tagsort;

% file = file(ismember(tag, tags));
% path = path(ismember(tag, tags));
% tag = tag(ismember(tag, tags));


%% Divide data sets into different power regions

%% 10uJ 
tags_10= tags(0<power & power<15);
delays_10= delays(0<power & power<15);
path_10 = path(0<power & power<15);
file_10 = file(0<power & power<15);
power_10 = power(0<power & power<15);

%% 20uJ 
tags_20= tags(15<power & power<35);
delays_20= delays(15<power & power<35);
path_20= path(15<power & power<35);
file_20= file(15<power & power<35);
power_20= power(15<power & power<35);

%% 100uJ 
tags_100= tags(power>80);
delays_100= delays(power>80);
path_100 = path(power>80);
file_100 = file(power>80);
power_100 = power(power>80);


%%
figure
histogram(power)
xlabel('power (uJ)')
title('Overall power distribution')

figure
histogram(delays(delays<1000 & delays>500),25)
xlim([500 1000])
xlabel('delays (fs)')
title('Overall time point distribution')

figure
histogram2(power, delays)
xlabel('power (uJ)')
ylabel('delays (fs)')
title('Overall power and time point distribution')

figure
histogram(delays_10)
xlabel('delays (fs)')
title('10uJ time point distribution')

figure
histogram(delays_20)
xlabel('delays (fs)')
title('20uJ time point distribution')

figure
histogram(delays_100)
xlabel('delays (fs)')
title('100uJ time point distribution')


%% Determine power and range to bin

tag = tags_100(delays_100<1000);
paths = path_100(delays_100<1000);
files = file_100(delays_100<1000);
delay = delays_100(delays_100<1000);
power_avrg = mean(power_100(delays_100<1000));

%% Determine parameters for binning and bin

binType = 'eqNpatterns'; %Possible options: 'eqNpatterns''eqWidthBins' 'binEdges'  
N_patterns = 14500;
width_timebin = 500;
bin_edges = [min(delay) 1000 max(delay)]; %Min and max values of delay times should always be the outer edges
if strcmp(binType,'eqWidthBins')
    path_out = [path_output sprintf('%0.0f_uJ_%s_%d_sub1ps/',power_avrg,binType,width_timebin)]; 
elseif strcmp(binType,'eqNpatterns')
    path_out = [path_output sprintf('%0.0f_uJ_%s_%d_sub1ps/',power_avrg,binType,N_patterns)]; 
elseif strcmp(binType,'binEdges')
    path_out = [path_output sprintf('%0.0f_uJ_%s_1000to3000/',power_avrg,binType)]; 
else
    print('Bin Type unknown')
    pause 
end

if ~exist(path_out)
    mkdir(sprintf(path_out))
end

[ table_properties ] = create_bins(delay, tag, paths,files, binType, path_out, N_patterns, width_timebin, bin_edges);

%%



