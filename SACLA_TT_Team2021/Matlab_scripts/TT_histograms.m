%% Read csv files from TT analysis and create histograms of the distribution
%% should loop over several csv and create 2D plot showing possible shifts


clear all 
clc

%% Determine directory where to find files
path = '~/Dropbox/SACLA_TT_Team2021/run260621/';
start_run = 1009100; %1009469	1009502
stop_run = 1009600;
pixel2fs = 2.4;
centerpixel = 890;
t_zero = 0;%centerpixel*pixel2fs; 
mopo0corr = 87;%from '~/libsfx/metaData/zeroMopo.dat'
pls2fs = 6.6666666667;

runs = [start_run:stop_run];
% tags = [];
% nominal_time=[];
% pixel=[];
% delays=[];

%% Load files 
for i=1:length(runs)
    if isfile([path sprintf('%d',runs(i)) '.csv'])
        A=readtable([path sprintf('%d',runs(i)) '.csv']);
        tags{i} = A.tag_number;
%         tag{i} = table2array(A(:,'tag_number'));
        TT_pixel{i} = (A.timing_edge_derivative_pixel_+A.timing_edge_derivative_pixel_)/2;
        t_corr{i} = (centerpixel-TT_pixel{i}).*pixel2fs;
%         TEDP = table2array(A(:,'timing_edge_derivative_pixel_'));
%         TEFP = table2array(A(:,'timing_edge_fitting_pixel_'));
        nominal_time{i} = A.xfel_bl_3_st_2_motor_1_position_pulse_;%
        %[nominal_time; table2array(A(:,'xfel_bl_3_st_2_motor_1_position_pulse_'))];
%         pixel = [pixel; (TEDP+TEFP)./2];
        delay{i} = nominal_time{i}*6.6666666667+t_corr{i}-mopo0corr*pls2fs;
        tags{i} = tags{i}(~isnan(TT_pixel{i}));
        delay{i} = delay{i}(~isnan(TT_pixel{i}));
        TT_pixel{i} = TT_pixel{i}(~isnan(TT_pixel{i}));
    end    
end

% delays = nominal_time*6.6666666667+pixel.*pixel2fs-t_zero-mopo0corr*pls2fs;
% fprintf("total: %d tags, of that %d NaNs were removed\n",length(tags),sum(isnan(pixel)));
%%

figure()
hold on
for i=1:length(runs)
    histogram(TT_pixel{i})
    xlabel('Pixel number')
end
figure()
hold on
for i=1:length(runs)
    histogram(delay{i})
    xlabel('Delay (fs)')
end


figure()
hold on
for i=1:length(runs)
    histogram2(TT_pixel{i},tags{i},'DisplayStyle','tile','ShowEmptyBins','on')
    xlabel('Pixel number')
end
