%% plot histogram for data bins

clear all
clc

dat = readtable('../time_binning/out/112_uJ_eqNpatterns_19500/properties.csv');

edges_lb = dat.timepoint - dat.width_bins./2;

edges_ub = dat.timepoint + dat.width_bins./2;

edges = [edges_lb(1); edges_ub];

N = dat.N_patterns;


figure
histogram('BinEdges',edges','BinCounts', N)

