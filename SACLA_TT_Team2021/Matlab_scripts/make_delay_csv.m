
clear all 
clc

%% Determine directory where to find files
path = '~/Dropbox/SACLA_TT_Team2021/run260621/';
path_out = '~/Dropbox/SACLA_TT_Team2021/out/';
path_power = '~/Dropbox/SACLA_TT_Team2021/power_corr/power_corr/';
start_run = 1009018; %1009469	1009502
stop_run = 1010047;
pixel2fs = 2.4;
centerpixel = 890;
t_zero = 0;%centerpixel*pixel2fs; 
mopo0corr = 87;%from '~/libsfx/metaData/zeroMopo.dat'
pls2fs = 6.6666666667;

runs = [start_run:stop_run];
% tags = [];
% nominal_time=[];
% pixel=[];
% delays=[];
power_miss = [];

%% Load files 
for i=1:length(runs)
    if isfile([path sprintf('%d',runs(i)) '.csv'])
        i
        A=readtable([path sprintf('%d',runs(i)) '.csv']);
        tags{i} = A.tag_number;
%         tag{i} = table2array(A(:,'tag_number'));
        TT_pixel{i} = (A.timing_edge_derivative_pixel_+A.timing_edge_derivative_pixel_)/2;
        t_corr{i} = (centerpixel-TT_pixel{i}).*pixel2fs;
        nominal_time{i} = A.xfel_bl_3_st_2_motor_1_position_pulse_;%
        delay{i} = nominal_time{i}*6.6666666667+t_corr{i}-mopo0corr*pls2fs;
        tags{i} = tags{i}(~isnan(TT_pixel{i}));
        delay{i} = delay{i}(~isnan(TT_pixel{i}));
        TT_pixel{i} = TT_pixel{i}(~isnan(TT_pixel{i}));
        if size(A,2)==10 
            PD_signal{i} = [A.xfel_bl_3_st_2_pd_user_4_fitting_peak_voltage_V_(~isnan(A.timing_edge_derivative_pixel_))];
            PD_gain{i} = [A.xfel_bl_3_st_2_pd_user_4_fitting_peak_gainC(~isnan(A.timing_edge_derivative_pixel_))];
            power_est{i} =  PD_signal{i}.*(PD_gain{i}./5).*10^12;
            out = table(tags{i}, delay{i},power_est{i});
            out.Properties.VariableNames={'tag_number' 'delay' 'power_est'};
            writetable(out, [path_out sprintf('%d_delay',runs(i)) '.csv'])
        elseif isfile([path_power sprintf('%d',runs(i)) '_diode.txt'])
            B=readtable([path_power sprintf('%d',runs(i)) '_diode.txt']);
            power_tags=B.Var1;
            PD_signal{i} = [B.Var3(ismember(power_tags,tags{i}))];
            PD_gain{i} = [B.Var4(ismember(power_tags,tags{i}))];
            power_est{i} =  PD_signal{i}.*(PD_gain{i}./5).*10^12;
            out = table(tags{i}, delay{i},power_est{i});
            out.Properties.VariableNames={'tag_number' 'delay' 'power_est'};
            writetable(out, [path_out sprintf('%d_delay',runs(i)) '.csv'])
        else
            power_miss = [power_miss runs(i)]
%             out = table(tags{i}, delay{i},power_est{i});
%             out.Properties.VariableNames={'tag_number' 'delay' 'power_est'};
%             writetable(out, [path_out sprintf('%d_delay',runs(i)) '.csv'])
        end
    end
    
end

sprintf('Power estimate missing for run %d', power_miss)
