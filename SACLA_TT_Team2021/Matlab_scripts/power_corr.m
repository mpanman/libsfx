%% Estimating the pump laser power using:
%% Estimate value (uJ) = PD signal x (Gain/5) x 10^12

clc
clear all

%% Determine directory where to find files
path_TT = '~/Dropbox/SACLA_TT_Team2021/run260621/';
path_power = '~/Dropbox/SACLA_TT_Team2021/power_corr/power_corr/';
start_run =  1009020;% 1009020; %	
stop_run = 1009803;%	1009041; %
pixel2fs = 2.4;
centerpixel = 890;
t_zero = centerpixel*pixel2fs; 
%mopo0corr = 48;%from '~/libsfx/metaData/zeroMopo.dat'
pls2fs = 6.6666666667;

runs = [start_run:stop_run];
tags = [];
nominal_time=[];
pixel=[];
PD_signal=[];
PD_gain=[];
delays=[];

%% Load files 
l=1;
for i=[start_run:stop_run]
    if isfile([path_power sprintf('%d',i) '_diode.txt'])
        B=readtable([path_power sprintf('%d',i) '_diode.txt']);
        tags = [tags; B.Var1];%[tags_TT; table2array(A(:,'tag_number'))];
        PD_signal = [PD_signal; B.Var3];
        PD_gain = [PD_gain; B.Var4];
        
    end    
end
power_est = PD_signal.*(PD_gain./5).*10^12;

%%
figure
plot(tags, power_est,'x')
xlabel('tag number')
ylabel('estim. power (uJ)')