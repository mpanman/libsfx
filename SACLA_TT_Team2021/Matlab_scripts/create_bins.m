function [ table_properties ] = create_bins(delays, tags, path, file, binType, outputPath, N_patterns, width_timebin, bin_edges)


%% Create Bins for 100 uJ

[tsort,idxs] = sort(delays);
tagsort = tags(idxs);
pathsort = path(idxs);
filesort = file(idxs);

if strcmp(binType,'eqNpatterns')

    %% binning according to no of patterns per bin

    N_bins = floor(length(tsort)/N_patterns);
    N_pattern = floor(length(tsort)/N_bins);

    for i=1:N_bins
        tp_bin{i} = tsort((i-1)*N_pattern+1:i*N_pattern);
        t_bin(i) = mean(tp_bin{i});
        tags_bin(i,:) = tagsort((i-1)*N_pattern+1:i*N_pattern);
        paths_bin{i} = pathsort((i-1)*N_pattern+1:i*N_pattern);
        files_bin{i} = filesort((i-1)*N_pattern+1:i*N_pattern);

        [mu(i), sig(i)] = normfit(tp_bin{i});
        N_tp(i) = length(tp_bin{i});
        width(i) = max(tp_bin{i})-min(tp_bin{i});

        table_tags = table(tags_bin(i,:)', paths_bin{i}', tp_bin{i});
        table_tags.Properties.VariableNames={'tag_number' 'path' 'timepoints'};
        writetable(table_tags, [outputPath sprintf('%0.0f_fs',t_bin(i)) '.csv'])
        fileID = fopen([outputPath sprintf('%0.0f_bin.txt', t_bin(i))], 'a');
        for j = 1:length(tp_bin{i})
            fprintf(fileID, '%s  %s %0.0fto%0.0ffs\n', string(paths_bin{i}(j)),string(files_bin{i}(j)), min(tp_bin{i}), max(tp_bin{i}));
        end
        fclose(fileID);
    end

    table_properties = table(t_bin', mu', sig', N_tp', width');
    table_properties.Properties.VariableNames={'timepoint' 'mu' 'sigma' 'N_patterns' 'width_bins'};
    writetable(table_properties, [outputPath sprintf('properties') '.csv'])
    
elseif strcmp(binType,'eqWidthBins')
    %% Binning according to time-range
    
    for i=1:floor(abs(max(tsort)-min(tsort))/width_timebin)
        tp_bint{i} = tsort(tsort>min(tsort)+(i-1)*width_timebin & tsort<min(tsort)+(i)*width_timebin);
        t_bint(i) = mean(tp_bint{i});
        tags_bint{i} = tagsort(tsort>min(tsort)+(i-1)*width_timebin & tsort<min(tsort)+(i)*width_timebin);
        paths_bint{i} = pathsort(tsort>min(tsort)+(i-1)*width_timebin & tsort<min(tsort)+(i)*width_timebin);
        files_bint{i} = filesort(tsort>min(tsort)+(i-1)*width_timebin & tsort<min(tsort)+(i)*width_timebin);


        [mut(i), sigt(i)] = normfit(tp_bint{i});
        N_tpt(i) = length(tp_bint{i});
        width(i) = max(tp_bint{i})-min(tp_bint{i});

        table_tags = table(tags_bin(i,:)', paths_bin{i}', tp_bin{i});
        table_tags.Properties.VariableNames={'tag_number' 'path' 'timepoints'};
        writetable(table_tags, [outputPath sprintf('%0.0f_fs',t_bin(i)) '.csv'])
        fileID = fopen([outputPath sprintf('%0.0f_bin.txt', t_bin(i))], 'a');
        for j = 1:length(tp_bin{i})
            fprintf(fileID, '%s  %s %0.0fto%0.0ffs\n', string(paths_bin{i}(j)),string(files_bin{i}(j)), min(t_bin(i)), max(t_bin(i)));
        end
        fclose(fileID);

    end

    table_properties = table(t_bint', mut', sigt', N_tpt', width');
    table_properties.Properties.VariableNames={'timepoint' 'mu' 'sigma' 'N_patterns' 'width_bins'};
    writetable(table_properties, [outputPath sprintf('properties') '.csv'])

elseif strcmp(binType,'binEdges') 
    %% Binning according to defined edges
    for i=1:length(bin_edges)-1
        tp_bin{i} = tsort(tsort>bin_edges(i) & tsort<bin_edges(i+1));
        t_bin(i) = mean(tp_bin{i});
        tags_bin{i} = tagsort(tsort>bin_edges(i) & tsort<bin_edges(i+1));
        paths_bin{i} = pathsort(tsort>bin_edges(i) & tsort<bin_edges(i+1));
        files_bin{i} = filesort(tsort>bin_edges(i) & tsort<bin_edges(i+1));


        [mut(i), sigt(i)] = normfit(tp_bin{i});
        N_tpt(i) = length(tp_bin{i});
        width(i) = max(tp_bin{i})-min(tp_bin{i});

        table_tags = table(tags_bin{i}, paths_bin{i}', tp_bin{i});
        table_tags.Properties.VariableNames={'tag_number' 'path' 'timepoints'};
        writetable(table_tags, [outputPath sprintf('%0.0f_fs',t_bin(i)) '.csv'])
        fileID = fopen([outputPath sprintf('%0.0f_bin.txt', t_bin(i))], 'a');
        for j = 1:length(tp_bin{i})
            fprintf(fileID, '%s  %s %0.0fto%0.0ffs\n', string(paths_bin{i}(j)),string(files_bin{i}(j)), min(t_bin(i)), max(t_bin(i)));
        end
        fclose(fileID);
    end

    table_properties = table(t_bin', mut', sigt', N_tpt', width');
    table_properties.Properties.VariableNames={'timepoint' 'mu' 'sigma' 'N_patterns' 'width_bins'};
    writetable(table_properties, [outputPath sprintf('properties') '.csv'])
    
else
    printf('Binning type not defined')

end