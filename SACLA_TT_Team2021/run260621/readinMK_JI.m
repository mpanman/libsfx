clear;

% runs = [1009158 1009167]; %800 fs 
% datname= '800fs';
% tnom = 800;
% mopo0 = 240;
% mopo0corr = 0;

runs = [1009169 1009195]; %540 fs 
datname= '540fs';
tnom = 540;
mopo0 = 168;
mopo0corr = 0;

% runs = [1009296 1009331]; %540 fs 
% datname= '540fs';
% tnom = 540;
% mopo0 = 168;
% mopo0corr = 0;

% runs = [1009333 1009342]; %540 fs 
% datname= '540fs';
% tnom = 540;
% mopo0 = 168;
% mopo0corr = 0;

%  runs = [1009343 1009350]; %run after XFEL went down
%  datname= '540';
%  tnom = 540;
%  mopo0=168;
%  mopo0corr = 0;

%  runs = [1009352 1009381]; %-660 fs
%  datname= 'minus660fs';
%  tnom = -660;
%  mopo0 = -12;
%  mopo0corr = 0;

%   runs = [1009382 1009421]; %-460 fs
%  datname= 'minus460fs';
%  tnom = -460;
%  mopo0 = 18;
%  mopo0corr = 0;
 
%  runs = [1009422 1009459]; %100 fs
%  datname= '100fs';
%  tnom = 100;
%  mopo0 = 102;
%  mopo0corr = 0;

%  runs = [1009460 1009502]; %300 fs
%  datname= '300fs';
%  tnom = 300;
%  mopo0 = 131;
%  mopo0corr = 0;

%   runs = [1009503 1009541]; %800 fs
%  datname= '800fs';
%  tnom = 800;
%  mopo0 = 207;
%  mopo0corr = 0;

%  runs = [1009542 1009573]; %-100 fs
%  datname= 'minus100fs';
%  tnom = -100;
%  mopo0 = 72;
%  mopo0corr = 0;

%  runs = [1009574 1009622]; %0 fs
%  datname= '0fs';
%  tnom = 0;
%  mopo0 = 87;
%  mopo0corr = 0;


cali.m = -1/2.4; %Pixel per fs
%cali.c = -2154.59; 
cali.inv = 2.4; % fs/pixel

pls2fs = 6.671;

centerpixel = 890; 

t0 = centerpixel ./ cali.m;

delays = [];
tags = [];
for countruns=0:1:(max(runs)-min(runs))
    T = table2array(readtable(join([string(countruns+min(runs)) '.csv'],''), 'HeaderLines',2));
    %delays(countruns+1,1:size(T,1)) = (T(:,2) - centerpixel)./cali.m;
    %tags(countruns+1,1:size(T,1)) = T(:,1);
    delays = [delays (tnom+((T(:,2)) ./cali.m)-(mopo0corr)*pls2fs-t0)'];
    tags = [tags T(:,1)'];
end
%
%t = reshape(delays,[size(delays,1)*size(delays,2) ,1]);
%tags = tags(tags,[size(tags,1)*size(tags,2) ,1]);


tags = tags(~isnan(delays));
fprintf("total: %d tags, of that %d NaNs were removed\n",length(tags),sum(isnan(delays)));
delays = delays(~isnan(delays));
%t = t(t~=0);


t = delays;
figure;
histogram(t,80);
[tsort,idxs] = sort(t);
[mu, sig] = normfit(tsort');
title(join(['histogram for ' datname '. ' string(round(mu)) ' fs +- ' string(round(sig)) 'fs'],''));
xlabel('Time (fs)')

line([mu-sig mu-sig], ylim,'Color','red','LineStyle',':');
line([mu+sig mu+sig], ylim,'Color','red','LineStyle',':');

saveas(gcf,[datname '.png']);
save([datname '.mat'],'tags','delays');

% % tsel = t(t>=mu-sig&t<=mu+sig);
% %
% % figure;
% % histogram(tsel,80);
% % [mu2, sig2] = normfit(tsel');
% %
% %      line([mu2-sig2 mu2-sig2], ylim,'Color','red','LineStyle',':');
%     line([mu2+sig2 mu2+sig2], ylim,'Color','red','LineStyle',':');





