clear;

% runs = [1009158 1009167]; %1 ps 
% datname= '1ps_26uJ';
% tnom = 1000;
% mopo0 = 240;
% mopo0corr = 0;
% 
% runs = [1009168 1009195]; %540 fs 
% datname= '540fs_13uJ';
% tnom = 540;
% mopo0 = 168;
% mopo0corr = 0;

% runs = [1009296 1009331]; %540 fs 
% datname= '540fs_a_11uJ';
% tnom = 540;
% mopo0 = 168;
% mopo0corr = 0;

% runs = [1009333 1009342]; %540 fs 
% datname= '540fs_b_11uJ';
% tnom = 540;
% mopo0 = 168;
% mopo0corr = 0;

%  runs = [1009343 1009350]; %540 fs
% datname= '540fs_c_11uJ';
% tnom = 540;
% mopo0 = 168;
% mopo0corr = 0;
% 
%  runs = [1009352 1009381]; %-660 fs
%  datname= 'minus660fs_11uJ';
%  tnom = -660;
%  mopo0 = -12;
%  mopo0corr = 0;
% % 
%   runs = [1009382 1009421]; %-460 fs
%  datname= 'minus460fs_11uJ';
%  tnom = -460;
%  mopo0 = 18;
%  mopo0corr = 0;
%  
%  runs = [1009422 1009459]; %100 fs
%  datname= '100fs_11uJ';
%  tnom = 100;
%  mopo0 = 102;
%  mopo0corr = 0;

%  runs = [1009460 1009502]; %300 fs
%  datname= '300fs_11uJ';
%  tnom = 300;
%  mopo0 = 131;
%  mopo0corr = 0;

%   runs = [1009503 1009541]; %800 fs
%  datname= '800fs_11uJ';
%  tnom = 800;
%  mopo0 = 207;
%  mopo0corr = 0;

%  runs = [1009542 1009573]; %-100 fs
%  datname= 'minus100fs_11uJ';
%  tnom = -100;
%  mopo0 = 72;
%  mopo0corr = 0;
% 
%  runs = [1009574 1009622]; %0 fs
%  datname= '0fs_10uJ';
%  tnom = 0;
%  mopo0 = 87;
%  mopo0corr = 0;

%  runs = [1009623 1009666]; %1 ps
%  datname= '1ps_21uJ';
%  tnom = 1000;
%  mopo0 = 237;
%  mopo0corr = 0;

%  runs = [1009667 1009702]; %500 fs
%  datname= '500fs_20uJ';
%  tnom = 500;
%  mopo0 = 161;
%  mopo0corr = 0;

%  runs = [1009703 1009722]; %300 fs
%  datname= '300fs_20uJ';
%  tnom = 300;
%  mopo0 = 132;
%  mopo0corr = 0;
% 
%  runs = [1009724 1009773]; %800 fs
%  datname= '800fs_20uJ';
%  tnom = 800;
%  mopo0 = 207;
%  mopo0corr = 0;

%  runs = [1009774 1009807]; %200 fs
%  datname= '200fs_21uJ';
%  tnom = 200;
%  mopo0 = 117;
%  mopo0corr = 0;

%  runs = [1009811 1009845]; %3 ps
%  datname= '3ps_100uJ';
%  tnom = 3000;
%  mopo0 = 537;
%  mopo0corr = 0;

%  runs = [1009846 1009871]; %1 ps PCM
%  datname= '1ps_15uJ_PCM';
%  tnom = 1000;
%  mopo0 = 236;
%  mopo0corr = 0;

%  runs = [1009872 1009907]; %300 fs
%  datname= '300fs_100uJ';
%  tnom = 300;
%  mopo0 = 131;
%  mopo0corr = 0;

%  runs = [1009908 1009940]; %500 fs
%  datname= '500fs_100uJ';
%  tnom = 500;
%  mopo0 = 161;
%  mopo0corr = 0;
% 
%  runs = [1009941 1009987]; %50 fs
%  datname= '50fs_100uJ';
%  tnom = 50;
%  mopo0 = 94;
%  mopo0corr = 0;

%  runs = [1009989 1010021]; %800 fs
%  datname= '800fs_100uJ';
%  tnom = 800;
%  mopo0 = 207;
%  mopo0corr = 0;
%  
%  runs = [1010022 1010047]; %250 fs
%  datname= '250fs_100uJ';
%  tnom = 250;
%  mopo0 = 124;
%  mopo0corr = 0;


cali.m = -1/2.4; %Pixel per fs
%cali.c = -2154.59; 
cali.inv = 2.4; % fs/pixel

pls2fs = 6.671;

centerpixel = 890; 

t0 = centerpixel ./ cali.m;

delays = [];
tags = [];
for countruns=0:1:(max(runs)-min(runs))
    T = table2array(readtable(join([string(countruns+min(runs)) '.csv'],''), 'HeaderLines',2));
    %delays(countruns+1,1:size(T,1)) = (T(:,2) - centerpixel)./cali.m;
    %tags(countruns+1,1:size(T,1)) = T(:,1);
    delays = [delays (tnom+((T(:,2)) ./cali.m)-(mopo0corr)*pls2fs-t0)'];
    tags = [tags T(:,1)'];
end
%
%t = reshape(delays,[size(delays,1)*size(delays,2) ,1]);
%tags = tags(tags,[size(tags,1)*size(tags,2) ,1]);

tags = tags(~isnan(delays));
fprintf("total: %d tags, of that %d NaNs were removed\n",length(tags),sum(isnan(delays)));
delays = delays(~isnan(delays));
%t = t(t~=0);


t = delays;
figure;
histogram(t,80);
[tsort,idxs] = sort(t);
[mu, sig] = normfit(tsort');
title(join(['histogram for ' datname '. ' string(round(mu)) ' fs +- ' string(round(sig)) 'fs'],''));
xlabel('Time (fs)')

line([mu-sig mu-sig], ylim,'Color','red','LineStyle',':');
line([mu+sig mu+sig], ylim,'Color','red','LineStyle',':');

saveas(gcf,[datname '.png']);
save([datname '.mat'],'tags','delays');

% % tsel = t(t>=mu-sig&t<=mu+sig);
% %
% % figure;
% % histogram(tsel,80);
% % [mu2, sig2] = normfit(tsel');
% %
% %      line([mu2-sig2 mu2-sig2], ylim,'Color','red','LineStyle',':');
%     line([mu2+sig2 mu2+sig2], ylim,'Color','red','LineStyle',':');

%% For combining histograms

% 100 uJ runs
clear all

% power = ['20uJ';'21uJ';'26uJ']; %select which power you want to combine
% label = '20to26uJ';

% power = ['11uJ';'10uJ';'20uJ';'21uJ';'26uJ']; %select which power you want to combine
% label = '10to26uJ';

power = ['100uJ']; %select which power you want to combine
label = '100uJ';

%if all data
% power = 'uJ';

[m,~] = size(power);
g=1;
pattern = (['*_' power(g,:)]);
bbs=dir([pattern '.mat']);

[n,~] = size(bbs);

for g=2:m
    pattern = (['*' power(g,:)]);
    bs=dir([pattern '.mat']);
    [nn,~] = size(bs);
    for gg=1:nn
        bbs(n+gg)=bs(gg);
    end
    [n,j] = size(bbs);
    if j>1
        bbs=bbs';
    end
end


[n,~] = size(bbs);

for i = 1:n
    dil = load(bbs(i).name);
    
    if i==1
    delay_comb = dil.delays;
        else
        delay_comb = [delay_comb dil.delays];
    end
end


t = delay_comb;
figure;
histogram(t,80);
title(['Histogram for ' label ' data']);
xlabel('Time (fs)')

savefig(label)
saveas(gcf,[label '.png']);

