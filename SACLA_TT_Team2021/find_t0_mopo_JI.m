clc
clear all

%% Determine directory where to find files
path_TT = '~/Dropbox/Projects/SACLA_TT_Team2021/run260621/';
path_moposcan = '~/Dropbox/Projects/SACLA_TT_Team2021/t0moposcan/';
start_run =   1009020; %	1009196;
stop_run = 	1009041; % 1009216;
pixel2fs = 2.4;
centerpixel = 890;
t_zero = centerpixel*pixel2fs; 
%mopo0corr = 48;%from '~/libsfx/metaData/zeroMopo.dat'
pls2fs = 6.6666666667;

runs = [start_run:stop_run];
tags_TT = [];
nominal_time=[];
pixel=[];
delays=[];

%% Load files 
l=1;
for i=[start_run:stop_run]
    if isfile([path_TT sprintf('%d',i) '.csv'])
        A=readtable([path_TT sprintf('%d',i) '.csv']);
        B=readtable([path_moposcan sprintf('%d',i) '_diode.txt']);
        tags_TT = A.tag_number;%[tags_TT; table2array(A(:,'tag_number'))];
        for ii = 1:length(tags_TT)
            mopo= B.Var2(B.Var1==tags_TT(ii));
            TT_pixel = (A.timing_edge_derivative_pixel_(A.tag_number==tags_TT(ii))+A.timing_edge_derivative_pixel_(A.tag_number==tags_TT(ii)))/2;
            TT_corr = (centerpixel-TT_pixel)*pixel2fs;
            TT_corr_mopo = TT_corr/pls2fs;
            mopo_corr(l) = mopo-TT_corr_mopo;
            intensity(l) = B.Var3(B.Var1==tags_TT(ii));
            l=l+1;
        end
        clear tags_TT mopo TT_pixel TT_corr 
%         tag{i} = table2array(A(:,'tag_number'));
%         TEDP = table2array(A(:,'timing_edge_derivative_pixel_'));
%         TEFP = table2array(A(:,'timing_edge_fitting_pixel_'));
%         nominal_time = [nominal_time; table2array(A(:,'xfel_bl_3_st_2_motor_1_position_pulse_'))];
%         pixel = [pixel; (TEDP+TEFP)./2];
        
    end    
end
 
intensity(isnan(mopo_corr(:))) =  [];
mopo_corr(isnan(mopo_corr(:))) = [];

%%PLot
figure
plot(mopo_corr,intensity,'x')
xlabel('Motor position')
ylabel('PD user 7')

%% Fit error function

fitfunc = 'A*(1+erf((x-x0)/(sigma*sqrt(2))))+o';
f = fit(mopo_corr',intensity',fitfunc,'Start',[0.2; 80; 1; 0.3])

%%
figure
hold on
plot(mopo_corr,intensity,'x')
plot(mopo_corr,f(mopo_corr),'-r')
xlabel('Motor position')
ylabel('PD user 7')
