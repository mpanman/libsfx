; Optimized panel offsets can be found at the end of the file
; Optimized panel offsets can be found at the end of the file
; CrystFEL geometry file produced by prepare_cheetah_api2.py
;   Takanori Nakane (tnakane@mrc-lmb.cam.ac.uk)
; Detector ID: MPCCD-8B0-2-006-1
; for tiled but NOT reassembled images (512x8192 pixels)

clen = 0.0494
res = 20000                 ; = 1 m /50 micron
;badrow_direction = x
;max_adu = 250000           ; should NOT be used. see best practice on CrystFEL's Web site
data = /%/data
;mask = /metadata/pixelmask ; this does not work in CrystFEL 0.6.2 (reported bug)
mask_good = 0x00            ; instead, we can specify bad regions below if necessary
mask_bad = 0xFF
photon_energy = /%/photon_energy_ev ; roughly 7020.0 eV

; Definitions for geoptimiser
rigid_group_q1 = q1
rigid_group_q2 = q2
rigid_group_q3 = q3
rigid_group_q4 = q4
rigid_group_q5 = q5
rigid_group_q6 = q6
rigid_group_q7 = q7
rigid_group_q8 = q8

rigid_group_collection_connected = q1,q2,q3,q4,q5,q6,q7,q8
rigid_group_collection_independent = q1,q2,q3,q4,q5,q6,q7,q8

; Panel definitions
; sensor MPCCD-8B0-2-006-1
q1/adu_per_eV = 0.001425
q1/min_fs = 0
q1/min_ss = 0
q1/max_fs = 511
q1/max_ss = 1023
q1/fs = +0.003480x -0.999994y
q1/ss = +0.999994x +0.003480y
q1/corner_x = 30.9253
q1/corner_y = 1033.93

; sensor MPCCD-8B0-2-006-2
q2/adu_per_eV = 0.001425
q2/min_fs = 0
q2/min_ss = 1024
q2/max_fs = 511
q2/max_ss = 2047
q2/fs = +0.000936x -1.000000y
q2/ss = +1.000000x +0.000936y
q2/corner_x = 33.0915
q2/corner_y = 500.411

; sensor MPCCD-8B0-2-006-3
q3/adu_per_eV = 0.001425
q3/min_fs = 0
q3/min_ss = 2048
q3/max_fs = 511
q3/max_ss = 3071
q3/fs = -0.001773x -0.999997y
q3/ss = +0.999997x -0.001773y
q3/corner_x = -16.7684
q3/corner_y = -33.6301

; sensor MPCCD-8B0-2-006-4
q4/adu_per_eV = 0.001425
q4/min_fs = 0
q4/min_ss = 3072
q4/max_fs = 511
q4/max_ss = 4095
q4/fs = -0.002948x -0.999996y
q4/ss = +0.999996x -0.002948y
q4/corner_x = -18.0768
q4/corner_y = -566.847

; sensor MPCCD-8B0-2-006-5
q5/adu_per_eV = 0.001425
q5/min_fs = 0
q5/min_ss = 4096
q5/max_fs = 511
q5/max_ss = 5119
q5/fs = -0.004155x +0.999991y
q5/ss = -0.999991x -0.004155y
q5/corner_x = 16.914
q5/corner_y = 572.967

; sensor MPCCD-8B0-2-006-6
q6/adu_per_eV = 0.001425
q6/min_fs = 0
q6/min_ss = 5120
q6/max_fs = 511
q6/max_ss = 6143
q6/fs = +0.000778x +1.000000y
q6/ss = -1.000000x +0.000778y
q6/corner_x = 15.8669
q6/corner_y = 36.6317

; sensor MPCCD-8B0-2-006-7
q7/adu_per_eV = 0.001425
q7/min_fs = 0
q7/min_ss = 6144
q7/max_fs = 511
q7/max_ss = 7167
q7/fs = +0.002115x +0.999998y
q7/ss = -0.999998x +0.002115y
q7/corner_x = -34.5372
q7/corner_y = -498.027

; sensor MPCCD-8B0-2-006-8
q8/adu_per_eV = 0.001425
q8/min_fs = 0
q8/min_ss = 7168
q8/max_fs = 511
q8/max_ss = 8191
q8/fs = +0.003654x +0.999994y
q8/ss = -0.999994x +0.003654y
q8/corner_x = -35.6989
q8/corner_y = -1031.31

; Bad regions near edges of each sensor
badv1/min_fs = 0
badv1/max_fs = 4
badv1/min_ss = 0
badv1/max_ss = 8191

badv2/min_fs = 507
badv2/max_fs = 511
badv2/min_ss = 0
badv2/max_ss = 8191

badq0h1/min_fs = 0
badq0h1/max_fs = 511
badq0h1/min_ss = 0
badq0h1/max_ss = 4

badq1h1/min_fs = 0
badq1h1/max_fs = 511
badq1h1/min_ss = 1024
badq1h1/max_ss = 1028

badq2h1/min_fs = 0
badq2h1/max_fs = 511
badq2h1/min_ss = 2048
badq2h1/max_ss = 2052

badq3h1/min_fs = 0
badq3h1/max_fs = 511
badq3h1/min_ss = 3072
badq3h1/max_ss = 3076

badq4h1/min_fs = 0
badq4h1/max_fs = 511
badq4h1/min_ss = 4096
badq4h1/max_ss = 4100

badq5h1/min_fs = 0
badq5h1/max_fs = 511
badq5h1/min_ss = 5120
badq5h1/max_ss = 5124

badq6h1/min_fs = 0
badq6h1/max_fs = 511
badq6h1/min_ss = 6144
badq6h1/max_ss = 6148

badq7h1/min_fs = 0
badq7h1/max_fs = 511
badq7h1/min_ss = 7168
badq7h1/max_ss = 7172

; Bad regions near outer edges of each sensor due to amplifier shields;
; you might want to optimize these widths (edit min_ss).
badq0h2/min_fs = 0
badq0h2/max_fs = 511
badq0h2/min_ss = 994
badq0h2/max_ss = 1023

badq1h2/min_fs = 0
badq1h2/max_fs = 511
badq1h2/min_ss = 2018
badq1h2/max_ss = 2047

badq2h2/min_fs = 0
badq2h2/max_fs = 511
badq2h2/min_ss = 3042
badq2h2/max_ss = 3071

badq3h2/min_fs = 0
badq3h2/max_fs = 511
badq3h2/min_ss = 4066
badq3h2/max_ss = 4095

badq4h2/min_fs = 0
badq4h2/max_fs = 511
badq4h2/min_ss = 5090
badq4h2/max_ss = 5119

badq5h2/min_fs = 0
badq5h2/max_fs = 511
badq5h2/min_ss = 6114
badq5h2/max_ss = 6143

badq6h2/min_fs = 0
badq6h2/max_fs = 511
badq6h2/min_ss = 7138
badq6h2/max_ss = 7167

badq7h2/min_fs = 0
badq7h2/max_fs = 511
badq7h2/min_ss = 8162
badq7h2/max_ss = 8191

;center

badregionA/min_x = -63.0
badregionA/max_x = +52.0
badregionA/min_y = -74.0
badregionA/max_y = +63.0


badregionB/min_x = -295.0
badregionB/max_x = -121.0
badregionB/min_y = -96.0
badregionB/max_y = +67.0

badregionC/min_x = 794.0
badregionC/max_x = 815.0
badregionC/min_y = 533.0
badregionC/max_y = 571.0




q1/coffset = -0.000118
q2/coffset = -0.000157
q3/coffset = -0.000185
q4/coffset = -0.000259
q5/coffset = 0.000064
q6/coffset = 0.000070
q7/coffset = -0.000015
q8/coffset = -0.000124
